package aplicativo.tech.saborbrasilrestaurante;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.controller.TaxasAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.UserSQLiteHandler;
import aplicativo.tech.saborbrasilrestaurante.model.Taxas;
import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import aplicativo.tech.saborbrasilrestaurante.webservice.ApiRetrofitLoginHttp;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Malcoln on 15/03/2018.
 */

public class RegisterActivity extends Fragment implements TaxasAdapter.AoClicarNoBairro{

    @BindView(R.id.appBar)
    AppBarLayout mAppBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnRegister)
    Button btnRegister;

    @BindView(R.id.name)
    EditText inputFullName;

    @BindView(R.id.email)
    EditText inputEmail;

    @BindView(R.id.password)
    EditText inputPassword;

    @BindView(R.id.input_User_telefone)
    EditText input_User_telefone;

    @BindView(R.id.input_User_endereco)
    EditText input_User_endereco;

    @BindView(R.id.input_User_bairro)
    TextView input_User_bairro;

    @BindView(R.id.input_user_referencia)
    EditText input_user_referencia;

    private ProgressDialog pDialog;
    private SessionManagerPreConfig session;
    private UserSQLiteHandler db;
    Fragment fragment;

    private List<Taxas> mTaxasList;
    RecyclerView recyclerView;
    TaxasAdapter taxasAdapter;
    public EditText txtSearch;
    Taxas bairro;
    static String strTaxa;
    public static String loopBairroNome;

    private Boolean validacaoBairro = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_register,container,false);

        ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        final ActionBar ab = (ActionBar)((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        //Metodo Static inicializa AsyncTask webService Json API de Bairros
        ChamarTaxasEntrega.listarTaxas();
        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManagerPreConfig(getActivity());

        // SQLite database handler
        db = new UserSQLiteHandler(getActivity());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // UserRetrofit is already logged in. Take him to PERFILACTIVITY
            Intent intent = new Intent(getActivity(),
                    MainActivity.class);
            startActivity(intent);

        }

        input_User_bairro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listarBairros();
            }
        });

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Boolean validacao1 = false;
                Boolean validacao2 = false;

                if (inputFullName.getText().toString().isEmpty()
                        || inputEmail.getText().toString().isEmpty() || inputPassword.getText().toString().isEmpty()
                        || input_User_telefone.getText().toString().isEmpty() || input_User_endereco.getText().toString().isEmpty()
                        || input_user_referencia.getText().toString().isEmpty()
                        || input_User_bairro.getText().toString().equals("Bairro")) {
                    Toast.makeText(getContext(),"Preencha todos os campos corretamente.",Toast.LENGTH_LONG).show();
                } else {
                    validacao1 = true;
                }
                if (8>input_User_telefone.getText().toString().length()
                        || 12<input_User_telefone.getText().toString().length()){
                    Toast.makeText(getContext(),"Seu telefone deve ter entre: 8 numeros (telefone local), " +
                            "9 numeros (celular) e até 12 numeros caso inclua o DDD",Toast.LENGTH_LONG).show();
                } else {
                    validacao2=true;
                }
                if (validacao1 && validacao2){
                    performRegistration();
                }
            }
        });

    return view;
    }

    private void listarBairros(){

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.taxa_recyclerview,null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setTitle("Bairros para entrega");
        alertDialogBuilder.setIcon(R.drawable.ic_moto_entrega);

        txtSearch= view.findViewById(R.id.mSearchView);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                mAppBar.setExpanded(true);

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newText = txtSearch.getText().toString();
                buscar(newText);
                mAppBar.setExpanded(false);
            }
            @Override
            public void afterTextChanged(Editable editable) {  }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setTag("web");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        taxasAtualizador();
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (txtSearch.getText().toString().equals("")
                                || validacaoBairro==false){

                            Toast.makeText(getActivity(), "Escolha um bairro válido", Toast.LENGTH_LONG).show();


                        } else {
                            input_User_bairro.setText(txtSearch.getText().toString());

                            dialogInterface.dismiss();
                        }
                    }
                });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void taxasAtualizador() {
        taxasAdapter = new TaxasAdapter(getActivity(), ChamarTaxasEntrega.mTaxasList);
        taxasAdapter.setAoClicarNoItem(this);
        recyclerView.setAdapter(taxasAdapter);
    }


    /* REGRAS USO FILTRO DE BUSCA */
    public void buscar(String src){
        if (src==null || src.trim().equals("")){
            taxasAtualizador();
            return;
        }
        List<Taxas> listEcontrada =  new ArrayList<>(ChamarTaxasEntrega.mTaxasList);

        for (int i= listEcontrada.size()-1;i>=0;i-- ) {
            Taxas newProd = listEcontrada.get(i);
            loopBairroNome = newProd.getNome().toUpperCase();
            if (!loopBairroNome.contains(src.toUpperCase())){
                listEcontrada.remove(newProd);
            }
            if (loopBairroNome.contains(src.toUpperCase())){
                strTaxa = newProd.getTaxa();
                loopBairroNome = newProd.getNome();

                if (src.equals(loopBairroNome)){
                    validacaoBairro = true;
                } else {
                    validacaoBairro = false;
                }

            }
        }
        TaxasAdapter adapter = new TaxasAdapter(getActivity(), listEcontrada);
        adapter.setAoClicarNoItem(this);
        recyclerView.setAdapter(adapter);
    }



    private void limparBusca() {
        taxasAtualizador();
    }


    /****************************************************************************
     *
     * REST Retrofit
     */

    private ApiRetrofitLoginHttp getApiLoginHttp(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ApiRetrofitLoginHttp apiRetrofitLoginHttp = retrofit.create(ApiRetrofitLoginHttp.class);

        return apiRetrofitLoginHttp;
    }

    public void performRegistration(){

        pDialog.setMessage("Registrando ...");
        showDialog();

        final String name = inputFullName.getText().toString().trim();
        final String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        final String endereco = input_User_endereco.getText().toString().trim();
        final String bairro = input_User_bairro.getText().toString().trim();
        final String telefone = input_User_telefone.getText().toString().trim();
        final String ponto_referencia = input_user_referencia.getText().toString().trim();
        ApiRetrofitLoginHttp apiRetrofitLoginHttp = this.getApiLoginHttp();

        if (name!="" || !name.isEmpty() || !name.startsWith("´") || email!="" || !email.isEmpty() || !ponto_referencia.isEmpty() ){

            Call<UserRetrofit> userCall = apiRetrofitLoginHttp.performRegistration(name,email, password, endereco, bairro, telefone, ponto_referencia);
            userCall.enqueue(new Callback<UserRetrofit>() {
                @Override
                public void onResponse(Call<UserRetrofit> call, retrofit2.Response<UserRetrofit> response) {
                    if (response.body().getResponse().equals("ok")){

                        Toast.makeText(getActivity(), "Registro Efetuado com sucesso", Toast.LENGTH_SHORT).show();
                        session.setLoginStatus(true);
                        session.setWriteName(name);
                        session.setDataUser(endereco, bairro, strTaxa, telefone, email, ponto_referencia);

                        UserSQLiteHandler userSQLiteHdler = new UserSQLiteHandler(getContext());
                        userSQLiteHdler.addUser(name,email, endereco, bairro, ponto_referencia, telefone);

                        HashMap<String,String> User = userSQLiteHdler.getUserDetails();

                        getFragmentManager().beginTransaction().replace(R.id.fragment_container, new PerfilFragment()).commit();

                    } else if (response.body().getResponse().equals("exist")){
                        Toast.makeText(getActivity(), "Usuário já registrado", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResponse().equals("error")){
                        Toast.makeText(getActivity(), "Algo deu errado", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserRetrofit> call, Throwable t) {
                    Toast.makeText(getActivity(), "Algo deu errado", Toast.LENGTH_SHORT).show();
                }
            });
        }

        inputFullName.setText("");
        inputEmail.setText("");
        inputPassword.setText("");
        input_User_bairro.setText("");
        input_user_referencia.setText("");

        hideDialog();

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void aoClicarNoItem(View view, int position, Taxas mTaxas) {
        input_User_bairro.setText(mTaxas.getNome());
        txtSearch.setTextColor(Color.BLUE);
        txtSearch.setText(mTaxas.getNome());
    }
}