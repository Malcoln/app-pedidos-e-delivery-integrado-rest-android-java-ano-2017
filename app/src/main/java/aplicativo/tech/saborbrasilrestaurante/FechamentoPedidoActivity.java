package aplicativo.tech.saborbrasilrestaurante;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.controller.CarrinhoFechamentoPedidoAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import aplicativo.tech.saborbrasilrestaurante.model.Taxas;
import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import aplicativo.tech.saborbrasilrestaurante.util.NetworkChangeReceiver;
import aplicativo.tech.saborbrasilrestaurante.webservice.ApiRetrofitPedido;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Malcoln on 11/03/2018.
 */

public class FechamentoPedidoActivity extends AppCompatActivity {

    @BindView (R.id.fechamentoActToolbar)
    Toolbar toolbar;

    @BindView(R.id.listPedidos)
    ListView listaPedidos;

    @BindView(R.id.strTotal)
    TextView strTotal;

    @BindView(R.id.strNome)
    TextView strNome;

    @BindView(R.id.strEnderecoEntrega)
    TextView strEnderecoEntrega;

    @BindView(R.id.strBairro)
    TextView strBairro;

    @BindView(R.id.strTelefone)
    TextView strTelefone;

    @BindView(R.id.strEmail)
    TextView strEmail;

    @BindView(R.id.strReferencia)
    TextView strReferencia;

    @BindView(R.id.empty)
    TextView carrinhoVazio;

    @BindView(R.id.chkRetiradaNoBalcao)
    CheckBox chkRetiradaBalcao;

    @BindView(R.id.chkDinheiro)
    CheckBox chkDinheiro;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.chkCartao)
    CheckBox chkCartao;

    @BindView(R.id.btnFechamentoPedido)
    Button btnFechamentoPedido;

    @BindView(R.id.btnZoomPedido)
    ImageButton btnZoonPedido;

    AlertCarrinho alertCarrinho;

    SessionManagerPreConfig session;

    CarrinhoFechamentoPedidoAdapter adapter;

    CarrinhoDBRepo carrinhoDBRepo;

    ProgressDialog pDialog;

    private String totalPedido;
    private String user_email;
    private String detalhes_pedido;
    private String detalhes_pagamento;
    private String nome;
    private String detalhes_entrega;
    private String detalhes_bairro;
    private String user_referencia;
    private String det_status;
    private String user_telefone;

    private static String saidaDetalhePedido;

    List<String> precosList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fechamento_activity);

        ChamarTaxasEntrega.listarTaxas();

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        carrinhoVazio.setVisibility(View.GONE);

        session = new SessionManagerPreConfig(FechamentoPedidoActivity.this);

        atualizarCarrinho();

        //calculoTotal();

        getFormulario();

        chkCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkCartao.isChecked()){
                    chkDinheiro.setChecked(false);
                    spinner.setVisibility(View.GONE);
                    detalhes_pagamento = "Cartao de Credito";
                } else if (!chkCartao.isChecked()){
                    chkDinheiro.setChecked(true);
                    spinner.setVisibility(View.VISIBLE);
                    detalhes_pagamento = "Dinheiro //Troco para:";
                }
            }
        });

        chkDinheiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detalhes_pagamento = "Dinheiro //Troco para:";
                if (chkDinheiro.isChecked()){
                    chkCartao.setChecked(false);
                    spinner.setVisibility(View.VISIBLE);
                }else if (!chkDinheiro.isChecked()){
                    chkCartao.setChecked(true);
                    spinner.setVisibility(View.GONE);
                } else if (chkDinheiro.isChecked() && chkCartao.isChecked()) {
                    chkCartao.setChecked(false);
                    spinner.setVisibility(View.VISIBLE);
                }
            }
        });

        det_status = "Entrega por Motoboy";

        chkRetiradaBalcao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chkRetiradaBalcao.isChecked()){
                    det_status = "RETIRADA NO BALCÃO";
                    totalPedido = String.valueOf(carrinhoDBRepo.getValorTotal())+"0"+" (Retirada no balcão)";
                    strTotal.setText( String.valueOf(totalPedido));
                }
                else if (!chkRetiradaBalcao.isChecked()){

                    calculoTotal();
                }
            }
        });


        pDialog = new ProgressDialog(getBaseContext());
        pDialog.setCancelable(false);

        spinner.setVisibility(View.GONE);


        precosList = new ArrayList<>();
        precosList.add("Troco ?");
        precosList.add("não preciso");
        precosList.add("R$15,00");
        precosList.add("R$20,00");
        precosList.add("R$25,00");
        precosList.add("R$30,00");
        precosList.add("R$35,00");
        precosList.add("R$40,00");
        precosList.add("R$45,00");
        precosList.add("R$50,00");
        precosList.add("R$100,00");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1,
                precosList);

        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String precos = parent.getItemAtPosition(position).toString();
                detalhes_pagamento = "Troco para " + precos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFechamentoPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**********************************
                 * VALIDAÇÃO LOGIN USUÁRIO
                 */
                if (chkCartao.isChecked()|| chkDinheiro.isChecked()){
                    getFormulario();
                    detalhes_pedido = setPedidoString();
                    performPedido();
                } else {
                    Toast.makeText(getBaseContext(),"Escolha a forma de Pagamento entre: Cartão ou Dinheiro", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnZoonPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertCarrinhoFechamento(FechamentoPedidoActivity.this).ExecCarrinho();
            }
        });

    }


    private void atualizarCarrinho(){
        carrinhoDBRepo = new CarrinhoDBRepo(FechamentoPedidoActivity.this);
        adapter = new CarrinhoFechamentoPedidoAdapter(FechamentoPedidoActivity.this, carrinhoDBRepo.getProdutos());
        listaPedidos.setAdapter(adapter);

        comparadorTaxa();

        verificaAtualizacaoVTotal();

    }

    private String setPedidoString(){

        carrinhoDBRepo = new CarrinhoDBRepo(FechamentoPedidoActivity.this);
        StringBuilder stringBuilder = new StringBuilder();
        List<ProdutosItem> stringList = carrinhoDBRepo.getPedidoFinal();
        saidaDetalhePedido = String.valueOf(stringBuilder.append(stringList).append("\n"));

        Log.e("SAIDA: ", saidaDetalhePedido);

        return saidaDetalhePedido;
    }

    private void comparadorTaxa(){
        //Metodo Static inicializa AsyncTask webService Json API de Bairros
        //necessario para metodo comparadorTaxas()


        if (ChamarTaxasEntrega.mTaxasList!=null) {
            List<Taxas> listTaxasJson = new ArrayList<>(ChamarTaxasEntrega.mTaxasList);

            for (int i = listTaxasJson.size() - 1; i >= 0; i--) {
                Taxas taxaAtualizadaPorBairro = listTaxasJson.get(i);
                String bairroEncontrado = taxaAtualizadaPorBairro.getNome();

                //comparacao Bairro por Json com Bairro Cadastrado em Registro de Cliente
                if (bairroEncontrado.equals(session.getBairro())) {
                    if (!taxaAtualizadaPorBairro.getTaxa().equals(session.getTaxaBairro())) {
                        session.setDataUser(session.getEnderecoFromUser(), session.getBairro(),
                                taxaAtualizadaPorBairro.getTaxa(), session.getTelefone(), session.getEmail(), session.getReferencia());
                    }
                }

            }

            calculoTotal();
        }


    }

    //DETALHES DO PEDIDO //////////////////////

    private void calculoTotal(){

        CarrinhoDBRepo carrinhoDBRepo = new CarrinhoDBRepo(getBaseContext());
        carrinhoDBRepo.getProdutos();

        String taxaEntrega = session.getTaxaBairro();

        Double totalTaxa = 0.0;
        try {

            if (taxaEntrega!="taxa entrega"){
                totalTaxa = setDoubleTaxaTotal(taxaEntrega);
            }
            else{
                totalTaxa = 0.0;
            }

        }catch (Exception ex){
            Toast.makeText(getApplicationContext(), String.valueOf(ex),Toast.LENGTH_LONG).show();
        }

            Double totalCarrinho = setDoubleTaxaTotal(carrinhoDBRepo.getValorTotal());

            Double totalCalculodouble = totalTaxa + totalCarrinho;

            totalPedido = String.valueOf(totalCalculodouble)+"0"+" (Ped.: "+ carrinhoDBRepo.getValorTotal()+"0 Entrega:"+ totalTaxa + "0)";


        strTotal.setText( String.valueOf(totalPedido));

        if (carrinhoDBRepo.getProdutos().size()==0){
            carrinhoVazio.setVisibility(View.VISIBLE);
            listaPedidos.setVisibility(View.GONE);
        } else {
            carrinhoVazio.setVisibility(View.GONE);
        }
    }

    private void verificaAtualizacaoVTotal(){
        if (strTotal.getText().toString().equals("atualizando...")){
            startActivity(new Intent(getApplicationContext(),FechamentoPedidoActivity.class));
        }
    }

    private Double setDoubleTaxaTotal(String string){

        Double saida = Double.parseDouble(string.replace("," , "."));
        return  saida;
    }

    public void getFormulario() {

        //HashMap<String,String> UserRetrofit = userSQL.getUserDetails();

        user_email = session.getEmail();
        user_telefone = session.getTelefone();
        nome= session.readNameFromUser();
        user_referencia = session.getReferencia();
        detalhes_entrega = session.getEnderecoFromUser();
        detalhes_bairro = session.getBairro();

        Fragment fragment;


        boolean validacao = false;
        try {
            if (!nome.isEmpty()&& !user_email.isEmpty() && !user_telefone.isEmpty() && !detalhes_entrega.isEmpty() && !detalhes_bairro.isEmpty() && !detalhes_bairro.isEmpty()) {
                strNome.setText(nome);
                strEmail.setText(user_email);
                strTelefone.setText(user_telefone);
                strEnderecoEntrega.setText(detalhes_entrega);
                strBairro.setText(detalhes_bairro);
                strReferencia.setText(user_referencia);

            }


        } catch (Exception ex){
            session.displayToast("Erro em validação");
        }

    }
    /****************************************************************************
     *
     * REST Retrofit
     */

    private ApiRetrofitPedido getApiPedidoHttp(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiRetrofitPedido apiRetrofitPedido = retrofit.create(ApiRetrofitPedido.class);
        return apiRetrofitPedido;
    }

    public void performPedido(){

        pDialog.setMessage("Pedido sendo enviado");


        /*
        detalhes pagamento e det status
         */


        Call<UserRetrofit> call = getApiPedidoHttp().performPedido(totalPedido, user_email, detalhes_pedido,detalhes_pagamento,nome,detalhes_entrega,detalhes_bairro,user_referencia,det_status,user_telefone);

        call.enqueue(new Callback<UserRetrofit>() {
            @Override
            public void onResponse(Call<UserRetrofit> call, Response<UserRetrofit> response) {

                if (response.body().getResponse().equals("ok")){
                    Toast.makeText(getBaseContext(), "Pedido Efetuado com sucesso", Toast.LENGTH_LONG).show();
                    Sair();

                } else if (response.body().getResponse().equals("error")){
                    Toast.makeText(getBaseContext(), "Algo deu errado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserRetrofit> call, Throwable t) {

            }
        });
    }


    public void Sair() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder
                .setMessage("OBRIGADO E VOLTE SEMPRE!")
                .setCancelable(false)
                .setPositiveButton("AVALIAR APP", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=aplicativo.tech.saborbrasilrestaurante");
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                            }
                        })
                .setNegativeButton("VOLTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                })
                .setNeutralButton("FECHAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());

                                System.exit(1);
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
