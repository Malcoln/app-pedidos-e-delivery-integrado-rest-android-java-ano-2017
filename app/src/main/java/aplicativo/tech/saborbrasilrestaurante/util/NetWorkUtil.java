package aplicativo.tech.saborbrasilrestaurante.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetWorkUtil {
        public static int TYPE_WIFI = 1;
        public static int TYPE_MOBILE = 2;
        public static int TYPE_NOT_CONNECTED = 0;


        public static int getConnectivityStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static String getConnectivityStatusString(Context context) {
            int conn = NetWorkUtil.getConnectivityStatus(context);
            String status = null;
            if (conn == NetWorkUtil.TYPE_WIFI) {
                status = "Wifi Habilitado";
            } else if (conn == NetWorkUtil.TYPE_MOBILE) {
                status = "Conexão Internet Habilitada";
            } else if (conn == NetWorkUtil.TYPE_NOT_CONNECTED) {
                status = "Sem Conexão com a Internet";
            }
            return status;
        }

}
