package aplicativo.tech.saborbrasilrestaurante;

import android.os.AsyncTask;

import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.model.CardapioDoDiaModel;
import aplicativo.tech.saborbrasilrestaurante.webservice.ProdutosHttp;

/**
 * Created by Malcoln on 23/03/2018.
 */

public class ChamarCardapioDoDia {

    public static DownLoadListaTask downLoadListaTask;
    public static List<CardapioDoDiaModel> cardapioDoDiaModelList;

    public static void listaCardapioCarregar(){
        DownLoadListaTask downLoadListaTask = new DownLoadListaTask();
        downLoadListaTask.execute();
    }

    private static class DownLoadListaTask extends AsyncTask<Void,Void, List<CardapioDoDiaModel>>{
        @Override
        protected List<CardapioDoDiaModel> doInBackground(Void... voids) {

            return ProdutosHttp.obterListaCardapioDoDia();
        }

        @Override
        protected void onPostExecute(List<CardapioDoDiaModel> cardapioDoDiaModels) {
            cardapioDoDiaModelList = cardapioDoDiaModels;
            super.onPostExecute(cardapioDoDiaModels);
        }
    }
}
