package aplicativo.tech.saborbrasilrestaurante.controller;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.R;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * **************Uso de licensas**
 * Copyright 2013 Jake Wharton

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 *
 * Created by Malcoln on 26/07/2017.
 */

public class ProdutosListAdapter extends RecyclerView.Adapter<ProdutosListAdapter.PromocaoViewHolder> {

    private Context mContext;

    private List<ProdutosItem> mProdutosItem;

    private AoClicarNaPromocaoListener mListener;



    public ProdutosListAdapter(Context ctx, List<ProdutosItem> produtosItems){
        mContext = ctx;
        mProdutosItem = produtosItems;
    }

    public void setAoCLicarNaPromocaoListener( AoClicarNaPromocaoListener clickPromocao){
        mListener = clickPromocao;
        notifyDataSetChanged();
    }

    @Override
    public PromocaoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemViewNovidades = LayoutInflater.from(mContext).inflate(R.layout.produtos_nalista, parent,false);

        PromocaoViewHolder pvh = new PromocaoViewHolder(itemViewNovidades);

        itemViewNovidades.setTag(pvh);

        itemViewNovidades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    PromocaoViewHolder pvh = (PromocaoViewHolder)view.getTag();
                    int position = pvh.getAdapterPosition();
                    mListener.aoClicarNoItemNovidades(view, position, mProdutosItem.get(position));
                }
            }
        });

        return pvh;
    }

    @Override
    public void onBindViewHolder(PromocaoViewHolder holder, int position) {
        ProdutosItem produtosItem = mProdutosItem.get(position);

        try{
            Picasso.with(mContext).load(produtosItem.foto).fit().into(holder.imagePropaganda);
            //holder.txtCategoria.setText(produtosItem.categoria);
            holder.txtNome.setText(produtosItem.nome);

            holder.txtDescricao.setText(produtosItem.getDescricao());
            holder.txtPrecoAtual.setText("R$"+ produtosItem.getPreco());


        } catch (Exception e){e.printStackTrace();}


    }
    public interface AoClicarNaPromocaoListener {
        void aoClicarNoItemNovidades(View view, int position, ProdutosItem produtosItem);

    }


    @Override
    public int getItemCount() {
        return mProdutosItem != null? mProdutosItem.size() : 0;
    }


    public static class PromocaoViewHolder extends RecyclerView.ViewHolder{
        @Nullable
        //@BindView(R.id.txtCategoria)
        //public TextView txtCategoria;
        @BindView(R.id.imagePropaganda)
        public ImageView imagePropaganda;

        @BindView(R.id.txtNomeProduto)
        public TextView txtNome;

        @BindView(R.id.txtPreco)
        public TextView txtDescricao;

        @BindView(R.id.txtPor)
        public TextView txtPrecoAtual;



        public PromocaoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

}