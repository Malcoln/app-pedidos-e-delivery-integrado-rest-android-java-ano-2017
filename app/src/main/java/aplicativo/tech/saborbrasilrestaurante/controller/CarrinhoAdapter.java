package aplicativo.tech.saborbrasilrestaurante.controller;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.AlertCarrinho;
import aplicativo.tech.saborbrasilrestaurante.R;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;


/**
 * Created by Malcoln on 06/01/2017.
 */

public class CarrinhoAdapter extends BaseAdapter {
    private Context ctx;
    private List<ProdutosItem> lista;
    SpannableString stringAtravessada;
    CarrinhoDBRepo carrinhoDBRepo;


    public CarrinhoAdapter(Context context, List<ProdutosItem> produtos) {

        this.ctx = context;
        this.lista = produtos;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void getPreco(int position){

        ProdutosItem produto = lista.get(position);
        CarrinhoDBRepo pedidosRepositorio = new CarrinhoDBRepo(ctx);
        pedidosRepositorio.atualizar(produto);
        pedidosRepositorio.excluir(produto);

        AlertCarrinho alertCarrinho = new AlertCarrinho(ctx);
        alertCarrinho.ExecCarrinho();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ProdutosItem produto = lista.get(position);

        CarrinhoAdapter.ViewHolder viewHolder = null;
        if (convertView == null){

            convertView = LayoutInflater.from(ctx).inflate(R.layout.carrinho_lista, null);
            viewHolder = new CarrinhoAdapter.ViewHolder();

            viewHolder.strNomeProduto = (TextView)convertView.findViewById(R.id.txtTitle_Name);
            viewHolder.strNomeProduto.setText(String.valueOf(produto.getNome()));

            viewHolder.txtPreco = (TextView)convertView.findViewById(R.id.strPrecoCarrinho);
            viewHolder.txtPreco.setText("R$"+String.valueOf(produto.getPreco()));

            viewHolder.strObservacaoPedido = (convertView.findViewById(R.id.strObservacaoPedido));
            viewHolder.strObservacaoPedido.setText(produto.getDetalhe_Pedido());

            viewHolder.btnExclusao = (Button)convertView.findViewById(R.id.btnExclusao);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CarrinhoAdapter.ViewHolder) convertView.getTag();
        }

        final CarrinhoAdapter.ViewHolder finalViewHolder = viewHolder;
        final ViewHolder finalViewHolder1 = viewHolder;
        viewHolder.btnExclusao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getPreco(position);

                CarrinhoDBRepo pedidosRepositorio = new CarrinhoDBRepo(ctx);

                if (lista.size()==1) {
                    CarrinhoDBRepo.consultQtd=0;
                    CarrinhoDBRepo.consultValTotal=0;
                    CarrinhoDBRepo.CONSULT_DESCRI_PEDIDO="";
                    pedidosRepositorio.getProdutos();
                    notifyDataSetChanged();
                }
                notifyDataSetChanged();
                //view.getContext().startActivity(new Intent(view.getContext(), FechamentoPedidoActivity.class));

                String saida = "Foram retirados: ";

                stringAtravessada = new SpannableString(
                        "REMOVIDO" /*INDEX 0 - 8 */
                );

                stringAtravessada.setSpan(new StrikethroughSpan(),0,8, Spanned.SPAN_INCLUSIVE_INCLUSIVE);


                finalViewHolder.strNomeProduto.setText(stringAtravessada);

                finalViewHolder.txtPreco.setVisibility(View.GONE);


                Toast.makeText(ctx, saida+produto.getNome()+" da lista"
                        , Toast.LENGTH_SHORT).show();

            }
        });

        return convertView;
    }
    public String stringStravessada(){
        SpannableString stringAtravessada = new SpannableString(
                "REMOVIDO" /*INDEX 43 - 51 */
        );

        stringAtravessada.setSpan(new StrikethroughSpan(),0,8, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return String.valueOf(stringAtravessada);
    }
    public class ViewHolder{
        TextView strNomeProduto;
        TextView txtPreco;
        TextView strObservacaoPedido;
        Button btnExclusao;

    }

}

