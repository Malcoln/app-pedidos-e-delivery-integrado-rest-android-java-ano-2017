package aplicativo.tech.saborbrasilrestaurante.controller;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import aplicativo.tech.saborbrasilrestaurante.R;
import aplicativo.tech.saborbrasilrestaurante.model.Taxas;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;

/**
 * Created by Malcoln on 22/03/2018.
 */

public class TaxasAdapter extends RecyclerView.Adapter<TaxasAdapter.TaxasAdapterViewHolder>{

    private Context context;
    private List<Taxas> mTaxas;
    private AoClicarNoBairro mListener;


    public TaxasAdapter(Context context, List<Taxas> taxas) {
        this.context = context;
        this.mTaxas = taxas;
    }

    @Override
    public TaxasAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View viewCardapio = LayoutInflater.from(context).inflate(R.layout.item_taxa,parent,false);
        TaxasAdapterViewHolder taxasViewHldr = new TaxasAdapterViewHolder(viewCardapio);
        viewCardapio.setTag(taxasViewHldr);
        viewCardapio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener!=null){
                    TaxasAdapterViewHolder taxasAdapterViewHolder = (TaxasAdapterViewHolder)view.getTag();
                    int position = taxasAdapterViewHolder.getAdapterPosition();
                    mListener.aoClicarNoItem(view, position, mTaxas.get(position));
                }
            }
        });


        return taxasViewHldr;

    }

    public void setAoClicarNoItem(AoClicarNoBairro aoClicarNoItem){
        mListener = aoClicarNoItem;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(TaxasAdapterViewHolder holder, int position) {
        Taxas taxas = mTaxas.get(position);
        holder.strBairro.setText(taxas.getNome());
        holder.strTaxa.setText(taxas.getTaxa());
        holder.strTaxa.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {

        return mTaxas != null ? mTaxas.size() : 0;
    }

    public interface AoClicarNoBairro{
        void aoClicarNoItem(View view, int position, Taxas mTaxas);
    }


    public static class TaxasAdapterViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.strBairro)
        TextView strBairro;
        @Nullable
        @BindView(R.id.strTaxa)
        TextView strTaxa;

        public TaxasAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
