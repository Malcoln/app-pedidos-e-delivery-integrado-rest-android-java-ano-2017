package aplicativo.tech.saborbrasilrestaurante.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.R;
import aplicativo.tech.saborbrasilrestaurante.model.CardapioDoDiaModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malcoln on 26/02/2018.
 */

public class CardapioListAdapter extends RecyclerView.Adapter<CardapioListAdapter.CardapioViewHolder>{
    Context context;
    private List<CardapioDoDiaModel> cardapioDoDiaModelList;

    public CardapioListAdapter(Context context, List<CardapioDoDiaModel> cardapioDoDiaModelList) {
        this.context = context;
        this.cardapioDoDiaModelList = cardapioDoDiaModelList;
    }


    @Override
    public CardapioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View viewCardapio = LayoutInflater.from(context).inflate(R.layout.cardapio_nalista,parent,false);
        CardapioViewHolder cardapioViewHolder = new CardapioViewHolder(viewCardapio);

        return cardapioViewHolder;
    }

    @Override
    public void onBindViewHolder(CardapioViewHolder holder, int position) {
        CardapioDoDiaModel cardapioDoDiaModel = cardapioDoDiaModelList.get(position);
        try{

            holder.strOpcao1.setText(cardapioDoDiaModel.getOpcao1());
            holder.strOpcao2.setText(cardapioDoDiaModel.getOpcao2());
            holder.strOpcao3.setText(cardapioDoDiaModel.getOpcao3());
            holder.strOpcao4.setText(cardapioDoDiaModel.getOpcao4());
            holder.strOpcao5.setText(cardapioDoDiaModel.getOpcao5());
            holder.strOpcao6.setText(cardapioDoDiaModel.getOpcao6());
            holder.strOpcao7.setText(cardapioDoDiaModel.getOpcao7());
            holder.strOpcao8.setText(cardapioDoDiaModel.getOpcao8());
            holder.strOpcao9.setText(cardapioDoDiaModel.getOpcao9());
            holder.strOpcao10.setText(cardapioDoDiaModel.getOpcao10());
            holder.strOpcao11.setText(cardapioDoDiaModel.getOpcao11());
            holder.strOpcao12.setText(cardapioDoDiaModel.getOpcao12());
            holder.strOpcao13.setText(cardapioDoDiaModel.getOpcao13());
            holder.strOpcao14.setText(cardapioDoDiaModel.getOpcao14());
            holder.strOpcao15.setText(cardapioDoDiaModel.getOpcao15());
            holder.strOpcao16.setText(cardapioDoDiaModel.getOpcao16());

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return cardapioDoDiaModelList != null ? cardapioDoDiaModelList.size() : 0;
    }

    public class CardapioViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.strOpcao1)
        TextView strOpcao1;
        @BindView(R.id.strOpcao2)
        TextView strOpcao2;
        @BindView(R.id.strOpcao3)
        TextView strOpcao3;
        @BindView(R.id.strOpcao4)
        TextView strOpcao4;
        @BindView(R.id.strOpcao5)
        TextView strOpcao5;
        @BindView(R.id.strOpcao6)
        TextView strOpcao6;
        @BindView(R.id.strOpcao7)
        TextView strOpcao7;
        @BindView(R.id.strOpcao8)
        TextView strOpcao8;
        @BindView(R.id.strOpcao9)
        TextView strOpcao9;
        @BindView(R.id.strOpcao10)
        TextView strOpcao10;
        @BindView(R.id.strOpcao11)
        TextView strOpcao11;
        @BindView(R.id.strOpcao12)
        TextView strOpcao12;
        @BindView(R.id.strOpcao13)
        TextView strOpcao13;
        @BindView(R.id.strOpcao14)
        TextView strOpcao14;
        @BindView(R.id.strOpcao15)
        TextView strOpcao15;
        @BindView(R.id.strOpcao16)
        TextView strOpcao16;


        public CardapioViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
