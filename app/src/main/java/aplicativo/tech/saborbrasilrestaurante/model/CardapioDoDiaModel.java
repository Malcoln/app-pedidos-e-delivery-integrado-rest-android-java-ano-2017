package aplicativo.tech.saborbrasilrestaurante.model;

import java.io.Serializable;

/**
 * Created by Malcoln on 26/02/2018.
 */

public class CardapioDoDiaModel implements Serializable{
    @Override
    public String toString() {
        return "CardapioDoDiaModel{" +
                "id='" + id + '\'' +
                ", opcao1='" + opcao1 + '\'' +
                ", opcao2='" + opcao2 + '\'' +
                ", opcao3='" + opcao3 + '\'' +
                ", opcao4='" + opcao4 + '\'' +
                ", opcao5='" + opcao5 + '\'' +
                ", opcao6='" + opcao6 + '\'' +
                ", opcao7='" + opcao7 + '\'' +
                ", opcao8='" + opcao8 + '\'' +
                ", opcao9='" + opcao9 + '\'' +
                ", opcao10='" + opcao10 + '\'' +
                ", opcao11='" + opcao11 + '\'' +
                ", opcao12='" + opcao12 + '\'' +
                ", opcao13='" + opcao13 + '\'' +
                ", opcao14='" + opcao14 + '\'' +
                ", opcao15='" + opcao15 + '\'' +
                ", opcao16='" + opcao16 + '\'' +
                '}';
    }

    String id;
    String opcao1;
    String opcao2;
    String opcao3;
    String opcao4;
    String opcao5;
    String opcao6;
    String opcao7;
    String opcao8;

    String opcao9;

    String opcao10;
    String opcao11;
    String opcao12;
    String opcao13;
    String opcao14;
    String opcao15;
    String opcao16;
    public String getOpcao1() {
        return opcao1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOpcao1(String opcao1) {
        this.opcao1 = opcao1;
    }

    public String getOpcao2() {
        return opcao2;
    }

    public void setOpcao2(String opcao2) {
        this.opcao2 = opcao2;
    }

    public String getOpcao3() {
        return opcao3;
    }

    public void setOpcao3(String opcao3) {
        this.opcao3 = opcao3;
    }

    public String getOpcao4() {
        return opcao4;
    }

    public void setOpcao4(String opcao4) {
        this.opcao4 = opcao4;
    }

    public String getOpcao5() {
        return opcao5;
    }

    public void setOpcao5(String opcao5) {
        this.opcao5 = opcao5;
    }

    public String getOpcao6() {
        return opcao6;
    }

    public void setOpcao6(String opcao6) {
        this.opcao6 = opcao6;
    }

    public String getOpcao7() {
        return opcao7;
    }

    public void setOpcao7(String opcao7) {
        this.opcao7 = opcao7;
    }

    public String getOpcao8() {
        return opcao8;
    }

    public void setOpcao8(String opcao8) {
        this.opcao8 = opcao8;
    }

    public String getOpcao9() {
        return opcao9;
    }

    public void setOpcao9(String opcao9) {
        this.opcao9 = opcao9;
    }

    public String getOpcao10() {
        return opcao10;
    }

    public void setOpcao10(String opcao10) {
        this.opcao10 = opcao10;
    }

    public String getOpcao11() {
        return opcao11;
    }

    public void setOpcao11(String opcao11) {
        this.opcao11 = opcao11;
    }

    public String getOpcao12() {
        return opcao12;
    }

    public void setOpcao12(String opcao12) {
        this.opcao12 = opcao12;
    }

    public String getOpcao13() {
        return opcao13;
    }

    public void setOpcao13(String opcao13) {
        this.opcao13 = opcao13;
    }

    public String getOpcao14() {
        return opcao14;
    }

    public void setOpcao14(String opcao14) {
        this.opcao14 = opcao14;
    }

    public String getOpcao15() {
        return opcao15;
    }

    public void setOpcao15(String opcao15) {
        this.opcao15 = opcao15;
    }

    public String getOpcao16() {
        return opcao16;
    }

    public void setOpcao16(String opcao16) {
        this.opcao16 = opcao16;
    }
}
