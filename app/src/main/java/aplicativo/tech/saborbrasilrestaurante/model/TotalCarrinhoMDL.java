package aplicativo.tech.saborbrasilrestaurante.model;

public class TotalCarrinhoMDL {

    double precoTotalCompra;

    public double getPrecoTotalCompra() {
        return precoTotalCompra;
    }

    public void setPrecoTotalCompra(double precoTotalCompra) {
        this.precoTotalCompra = precoTotalCompra;
    }
}
