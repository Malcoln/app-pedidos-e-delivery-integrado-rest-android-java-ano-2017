package aplicativo.tech.saborbrasilrestaurante.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Malcoln on 22/03/2018.
 */

public class Taxas implements Serializable{


    private String nome;
    private String taxa;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTaxa(String taxa) {
        this.taxa = taxa;
    }

    @Override
    public String toString() {
        return "Taxas{" +
                "nome='" + nome + '\'' +
                ", taxa='" + taxa + '\'' +
                '}';
    }

    public String getNome() {
        return nome;
    }

    public String getTaxa() {
        return taxa;
    }
}
