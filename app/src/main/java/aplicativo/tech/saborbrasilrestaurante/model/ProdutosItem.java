package aplicativo.tech.saborbrasilrestaurante.model;

import java.io.Serializable;

/**
 * Created by Malcoln on 01/11/2017.
 */

public class ProdutosItem implements Serializable {
    long id;
    public String nome;
    public String descricao;
    public String foto;
    public String categoria;
    public String detalhe_Pedido;

    public String preco;
    public String total_Compra;


    public String getTotal_Compra() {
        return total_Compra;
    }

    public void setTotal_Compra(String total_Compra) {
        this.total_Compra = total_Compra;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String[] fotos;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getDetalhe_Pedido() { return detalhe_Pedido; }

    public void setDetalhe_Pedido(String detalhe_Pedido) {  this.detalhe_Pedido = detalhe_Pedido; }


    @Override
    public String toString() {
        return
                nome + ", detalhe Pedido= " + detalhe_Pedido + ", preco= " + preco ;
    }
}
