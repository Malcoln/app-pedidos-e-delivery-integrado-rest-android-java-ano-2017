package aplicativo.tech.saborbrasilrestaurante.model;

import java.io.Serializable;

/**
 * Created by Malcoln on 02/01/2018.
 */

public class WishModelProduct implements Serializable{

    private String id;
    private String title_name;
    private String descric;
    private String caminhoFoto;
    private boolean favoritoExist=false;

    public boolean isFavoritoExist() {
        return favoritoExist;
    }

    public void setFavoritoExist(boolean favoritoExist) {
        this.favoritoExist = favoritoExist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    public String getDescric() {
        return descric;
    }

    public void setDescric(String descric) {
        this.descric = descric;
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String caminhoFoto) {
        this.caminhoFoto = caminhoFoto;
    }

}
