package aplicativo.tech.saborbrasilrestaurante.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Malcoln on 11/12/2017.
 */

public class Promocoes implements Serializable{

    public String[] id;
    public String[] nome;
    public String foto;
    public String[] fotos;
    public String[] Preco;
    private String[] preco_estava;

    public String[] getPreco() {
        return Preco;
    }

    public void setPreco(String[] preco) {
        Preco = preco;
    }

    public String[] getId() {
        return id;
    }

    public void setId(String[] id) {
        this.id = id;
    }

    public String[] getNome() {
        return nome;
    }

    public void setNome(String[] nome) {
        this.nome = nome;
    }

    public String[] getFotos() {
        return fotos;
    }

    public void setFotos(String[] fotos) {
        this.fotos = fotos;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String[] getPreco_estava() {    return preco_estava;
    }

    public void setPreco_estava(String[] preco_estava) {   this.preco_estava = preco_estava;
    }

    @Override
    public String toString() {
        return "Promocoes{" +
                "id=" + Arrays.toString(id) +
                ", nome=" + Arrays.toString(nome) +
                ", foto='" + foto + '\'' +
                ", fotos=" + Arrays.toString(fotos) +
                ", preco_estava=" + Arrays.toString(preco_estava) +
                ", Preco=" + Arrays.toString(Preco) +
                '}';
    }
}
