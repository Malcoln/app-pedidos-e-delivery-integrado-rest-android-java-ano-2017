package aplicativo.tech.saborbrasilrestaurante.model;

import java.io.Serializable;

/**
 * Created by Malcoln on 23/12/2017.
 */

public class mdlFeedFace implements Serializable {

    private String id;
    private String from;
    private String link;
    private String name;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
    private String time;
    private String attachments_src_img;
    private String comments_created;
    private String comments_message;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAttachments_src_img() {
        return attachments_src_img;
    }

    public void setAttachments_src_img(String attachments_src_img) {
        this.attachments_src_img = attachments_src_img;
    }

    public String getComments_created() {
        return comments_created;
    }

    public void setComments_created(String comments_created) {
        this.comments_created = comments_created;
    }

    public String getComments_message() {
        return comments_message;
    }

    public void setComments_message(String comments_message) {
        this.comments_message = comments_message;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "mdlFeedFace{" +
                "id='" + id + '\'' +
                ", from='" + from + '\'' +
                ", link='" + link + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", time='" + time + '\'' +
                ", attachments_src_img='" + attachments_src_img + '\'' +
                ", comments_created='" + comments_created + '\'' +
                ", comments_message='" + comments_message + '\'' +
                '}';
    }
}
