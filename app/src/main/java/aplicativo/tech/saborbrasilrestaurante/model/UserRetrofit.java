package aplicativo.tech.saborbrasilrestaurante.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Malcoln on 16/03/2018.
 */

public class UserRetrofit {

    @SerializedName("response")
    private String Response;

    @SerializedName("name")
    private String Name;

    @SerializedName("login")
    private String Login;

    @SerializedName("endereco")
    private String endereco;

    @SerializedName("bairro")
    private String bairro;

    @SerializedName("telefone")
    private String telefone;

    @SerializedName("referencia")
    private String referencia;

    @SerializedName("tipo")
    private String senha;

    public String getSenha() { return senha; }

    public String getResponse() {
        return Response;
    }

    public String getName() {
        return Name;
    }

    public String getLogin() {
        return Login;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getReferencia() {
        return referencia;
    }
}
