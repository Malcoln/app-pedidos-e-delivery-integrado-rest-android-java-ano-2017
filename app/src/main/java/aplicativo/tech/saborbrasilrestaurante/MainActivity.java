package aplicativo.tech.saborbrasilrestaurante;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.BottomBar.BottomBar;
import aplicativo.tech.saborbrasilrestaurante.BottomBar.OnMenuTabClickListener;
import aplicativo.tech.saborbrasilrestaurante.controller.CardapioMainActivityAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import aplicativo.tech.saborbrasilrestaurante.util.NetworkChangeReceiver;


public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private BottomBar mBottomBar;

    private CarrinhoDBRepo carrinhoDBRepo;

    private List<ProdutosItem> lista;
    private NavigationView navigationView;

    SessionManagerPreConfig sessionManagerPreConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.navigation_view);

        RelativeLayout relativeLayout = findViewById(R.id.naviDrawerRelativeLayout);



        ChamarCardapioDoDia.listaCardapioCarregar();

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        sessionManagerPreConfig = new SessionManagerPreConfig(MainActivity.this);

        String nome = sessionManagerPreConfig.readNameFromUser();
        String email = sessionManagerPreConfig.getEmail();


        if (navigationView != null){
            setupDrawerContent(navigationView);

        }

        sessionManagerPreConfig = new SessionManagerPreConfig(MainActivity.this);


        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.setItemsFromMenu(R.menu.bottombar_menu, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
               // mMessageView.setText(getMessage(menuItemId, false));

                Fragment fragment = null;
                getAllFunctions(menuItemId);
            }
            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                getAllFunctions(menuItemId);
            }
        });

        // Setting colors for different tabs when there's more than three of them.
        // You can set colors for tabs in three different ways as shown below.
        mBottomBar.mapColorForTab(0, "#FF9b53");
        mBottomBar.mapColorForTab(1, "#FF0303");
        mBottomBar.mapColorForTab(2, "#FF6AE497");
        mBottomBar.mapColorForTab(3, "#7B1FA2");

        //Atualiza/Zera DB-SQLite no caso de interrupção do app
        atualizaSQLITE_DB();

        //spannableString = new SpannableString(strMenuSpannable);
    }


    private void getAllFunctions(@IdRes int menuItemId) {
        //Promocoes promocoes = PromocoesHttp.promocoes; //INSERIR WEBSERVICE PARA FORMULÁRIO DO PAINEL ADMINISTRATIVO
        Fragment fragment;
        if (menuItemId == R.id.bb_menu_catalogo) {
            fragment = new ProdutosListWebServiceFragment();
            FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
            ft1.replace(R.id.fragment_container, fragment);
            ft1.commit();
        } else if (menuItemId == R.id.bb_menu_carrinho){
            AlertCarrinho alertCarrinho = new AlertCarrinho(MainActivity.this);
            alertCarrinho.ExecCarrinho();
        } else if (menuItemId == R.id.bb_menu_cardapiodia){
            getCardapioDoDia();
        }

        else if (menuItemId == R.id.bb_menu_recents) {
            SessionManagerPreConfig session = new SessionManagerPreConfig(getBaseContext());
            if (session.isLoggedIn()){
                fragment = new PerfilFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }else {
                fragment = new LoginFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.commit();

            }

        } else if (menuItemId == R.id.bb_menu_contato) {
            Uri uri = Uri.parse("smsto:" + "19984120809");
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, uri);
            //sendIntent.putExtra(Intent.EXTRA_TEXT, "Contato para Negócios");
            //sendIntent.setType("text/plain");
            sendIntent.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(sendIntent, ""));
            //finish();
        } else if (menuItemId == R.id.bb_menu_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            Uri uriCall = Uri.parse("tel:" + "01938915443");
            intent.setData(uriCall);
            startActivity(intent);
            //onStart();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IsOnline isOnline = new IsOnline(getBaseContext());
        isOnline.isOn();
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.onReceive(getBaseContext(),new Intent());
    }

    @Override
    protected void onPause() {
        super.onPause();
        IsOnline isOnline = new IsOnline(getBaseContext());
        isOnline.isOn();
    }


    /***************************************************************
    ***************************************************************
    **Metodo para zerar DB no caso de interrupção do app e reinicio
     * ****/

    protected void atualizaSQLITE_DB(){
        carrinhoDBRepo = new CarrinhoDBRepo(getBaseContext());
        int contArmazena = carrinhoDBRepo.getProdutos().size();
        String totalCompra = carrinhoDBRepo.getValorTotal();
        lista = carrinhoDBRepo.getProdutos();

        if (contArmazena>0 && totalCompra.equals("0.0")){
            for (int k=0; k<contArmazena;k++){
                ProdutosItem produtosItem = lista.get(k);
                carrinhoDBRepo.excluir(produtosItem);
            }
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        //mBottomBar.onSaveInstanceState(outState);
    }
    private void setupDrawerContent(NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                getAllFunctions(menuItem.getItemId());

                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    /* **********************************************************************************************
    MÉTODOS PARA INSTANCIAR MENU SUPERIOR - onCreateOptionsMenu
    ATUALIZA ICONE DO CARRINHO PARA VAZIO/CHEIO
     ***********************************************************************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        //MenuItem menuItem = menu.findItem(R.id.action_wish);
        MenuItem contador = menu.findItem(R.id.strCont);

        verificaRepositorio(contador);

        return true;
    }


    /* **********************************************************************************************
    ATUALIZA REPOSITÓRIO CARRINHO / PEDIDOS
    ATUALIZA ICONE DO CARRINHO PARA VAZIO/CHEIO
     ***********************************************************************************************/
    private void verificaRepositorio(MenuItem contador) {
        carrinhoDBRepo = new CarrinhoDBRepo(getBaseContext());
        int contArmazena = carrinhoDBRepo.getProdutos().size();
        String totalCompra = carrinhoDBRepo.getValorTotal();

        if (contArmazena>0 && totalCompra=="0.0"){
            ProdutosItem produtosItem = new ProdutosItem();
            for (int k=0; k<contArmazena;k++){
                carrinhoDBRepo.excluir(produtosItem);
            }
        } else
        if (contArmazena>0){
            String saidaMenu = " ";
            if (contArmazena == 1){

                saidaMenu = "R$"+totalCompra+"0 -1 ITEM";

            } else if (contArmazena > 1){

                saidaMenu = "R$"+totalCompra+"0 -"+contArmazena + " ITENS";

            }
            contador.setTitle(saidaMenu);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.strCont){
            AlertCarrinho alertCarrinho = new AlertCarrinho(MainActivity.this);
            alertCarrinho.ExecCarrinho();
        }
        if (id == R.id.avaliar){
            Avaliar();
        }

        if (id == R.id.action_exit){
            Sair();
        }
        switch (id){
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)){
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getCardapioDoDia() {

        CardapioMainActivityAdapter cardapioAdapter = new CardapioMainActivityAdapter(MainActivity.this,ChamarCardapioDoDia.cardapioDoDiaModelList);
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        View view = View.inflate(getBaseContext(), R.layout.cardapio_listview,null);
        RecyclerView recyclerView = view.findViewById(R.id.lv_cardapio);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(cardapioAdapter);
        alert.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
        ;
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

    public void Sair() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Sair do App");
        alertDialogBuilder

                .setCancelable(false)
                .setNegativeButton("VOLTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                })

                .setPositiveButton("SAIR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void Avaliar() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Compartilhar ou Avaliar App");
        alertDialogBuilder

                .setCancelable(false)
                .setNegativeButton("VOLTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                })

                .setPositiveButton("COMPARTILHAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "Baixe o app Sabor Brasil Restaurante : \n"+"https://play.google.com/store/apps/details?id=aplicativo.tech.saborbrasilrestaurante");
                                sendIntent.setType("text/plain");
                                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.app_name)+" indicação do app:"));
                            }
                        })
                .setNeutralButton("AVALIAR APP", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=aplicativo.tech.saborbrasilrestaurante");
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
