package aplicativo.tech.saborbrasilrestaurante.interfaces;

public interface VerificaLogOn {

    void getFormulario(String nome, String email, String telefone, String endereco, String bairro, String referencia);
}
