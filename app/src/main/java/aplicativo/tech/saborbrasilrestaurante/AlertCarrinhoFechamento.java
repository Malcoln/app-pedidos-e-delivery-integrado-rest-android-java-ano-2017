package aplicativo.tech.saborbrasilrestaurante;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import aplicativo.tech.saborbrasilrestaurante.controller.CarrinhoAdapter;
import aplicativo.tech.saborbrasilrestaurante.controller.CarrinhoFechamentoPedidoAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;


public class AlertCarrinhoFechamento extends Application {

    CarrinhoFechamentoPedidoAdapter carrinhoAdapter;

    CarrinhoDBRepo carrinhoDBRepo;

    Context context;

    TextView empty;

    SessionManagerPreConfig sessionManagerPreConfig;

    public AlertCarrinhoFechamento(Context context) {
        this.context = context;
    }

    public void ExecCarrinho() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.listview_fechamento_pedidos,null);

        empty = view.findViewById(R.id.empty);
        empty.setVisibility(View.GONE);

        sessionManagerPreConfig = new SessionManagerPreConfig(context);

        carrinhoDBRepo = new CarrinhoDBRepo(context);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setTitle("Itens do pedido");
        alertDialogBuilder.setIcon(R.drawable.botao_carrinho32);

        LinearLayout lineartotalPedido = view.findViewById(R.id.linearTotalPedido);
        lineartotalPedido.setVisibility(view.getVisibility());
        TextView strTotalPedido = view.findViewById(R.id.txtValor);
        strTotalPedido.setText("R$"+carrinhoDBRepo.getValorTotal()+"0");


        ListView listView = (ListView)view.findViewById(R.id.listPedidos);

        //VERIFICA VALOR DE CARRINHO E QUANTIDADE DE PRODUTO E INSTANCIA LISTA COM MENSAGEM DE VAZIA
        if (!carrinhoDBRepo.getValorTotal().equals("0.0")) {
            carrinhoAdapter = new CarrinhoFechamentoPedidoAdapter(context, carrinhoDBRepo.getProdutos());
            listView.setAdapter(carrinhoAdapter);
        }
        else if (carrinhoDBRepo.getValorTotal().equals("0.0")){
            ProdutosItem produtosItem = new ProdutosItem();
            for (int k=0; k<carrinhoDBRepo.getProdutos().size();k++){

                carrinhoDBRepo.excluir(produtosItem);
            }
            empty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
