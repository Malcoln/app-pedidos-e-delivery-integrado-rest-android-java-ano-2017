package aplicativo.tech.saborbrasilrestaurante;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


/**
 * Created by Malcoln on 16/12/2017.
 */

class MySingleton {

    private static MySingleton ourInstance;
    private static Context context;
    private RequestQueue requestQueue;

    private MySingleton(Context context){
        this.context = context;
        requestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue()
    {
        if (requestQueue==null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized MySingleton getInstance(Context context)
    {
        if(ourInstance==null){
            ourInstance = new MySingleton(context);
        }
        return ourInstance;
    }

    public <T>void addToRequestQue(Request<T> request){
        getRequestQueue().add(request);
    }
}
