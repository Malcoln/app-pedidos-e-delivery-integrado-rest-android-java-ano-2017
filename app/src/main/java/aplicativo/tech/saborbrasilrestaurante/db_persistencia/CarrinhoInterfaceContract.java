package aplicativo.tech.saborbrasilrestaurante.db_persistencia;

import android.provider.BaseColumns;

/**
 * Created by Malcoln on 31/12/2017.
 */

public interface CarrinhoInterfaceContract extends BaseColumns {

    String TABLE_PRODUCT = "product";
    String COL_PRODUCT_ID = "product_id";
    String COL_TITLENOME = "title_name";
    String COL_DESCRIC = "descric";
    String COL_PRECO = "preco";
    String COL_IMAGE = "caminhoImg";
    String COL_TOTAL_COMPRA = "total_compra";
    String COL_DETALHE_PEDIDO = "detalhe_compra";
}
