package aplicativo.tech.saborbrasilrestaurante.db_persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Malcoln on 13/12/2017.
 */

public class CarrinhoSQLHelper extends SQLiteOpenHelper{

    public static final String NOME_BANCO = "publicacaoDB";
    public static final int VERSAO_BANCO = 1;

    public CarrinhoSQLHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase dbPromocao) {

        String sql = "CREATE TABLE "+ CarrinhoInterfaceContract.TABLE_PRODUCT +"("
                + CarrinhoInterfaceContract.COL_PRODUCT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"
                + CarrinhoInterfaceContract.COL_TITLENOME + " TEXT NOT NULL ,"
                + CarrinhoInterfaceContract.COL_DESCRIC +" TEXT,"
                + CarrinhoInterfaceContract.COL_PRECO + " TEXT,"
                + CarrinhoInterfaceContract.COL_IMAGE + " TEXT,"
                + CarrinhoInterfaceContract.COL_TOTAL_COMPRA + " TEXT,"
                + CarrinhoInterfaceContract.COL_DETALHE_PEDIDO + " TEXT)";
        dbPromocao.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //String sql;
        /*
        switch (oldVersion){
            case 1:
                sql = "ALTER TABLE publicacao ADD COLUMN caminhoImg TEXT";
                sqLiteDatabase.execSQL(sql);

            /** Exemplo para atualização case VERSÔES do APP
             case 2:
             sql = "ALTER TABLE Pessoa ADD COLUMN cpf TEXT";
             db.execSQL(sql);

        } */
        if (oldVersion ==1){
            if (newVersion == 2){
                //dbPedido.execSQL("ALTER TABLE pedido ADD blablabla ");
            }
        }
    }
}
