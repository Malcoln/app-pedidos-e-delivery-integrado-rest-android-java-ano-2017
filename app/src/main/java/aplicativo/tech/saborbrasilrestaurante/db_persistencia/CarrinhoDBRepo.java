package aplicativo.tech.saborbrasilrestaurante.db_persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import aplicativo.tech.saborbrasilrestaurante.model.Promocoes;


/**
 * Created by Malcoln on 13/12/2017.
 */

public class CarrinhoDBRepo {
    List<Promocoes> mPromocoes;
    public static int consultQtd;
    public static double consultValTotal;
    public static String CONSULT_DESCRI_PEDIDO;
    private int acumQuant;
    private String PEDIDO_FINAL_STRING="";
    private static double acumValorTotal;
    private String acumDescriPed;
    static double totalPedido;
    public static double totalFechaCompra ;

    double precos;

   private CarrinhoSQLHelper carrinhoSQLHelper;

    public CarrinhoDBRepo(Context context) {
        this.carrinhoSQLHelper = new CarrinhoSQLHelper(context.getApplicationContext());
    }

    public long insert(ProdutosItem produtosItem){

        acumValorTotal += Double.parseDouble(produtosItem.getPreco());

        SQLiteDatabase db = carrinhoSQLHelper.getWritableDatabase();
        ContentValues dados = new ContentValues();
        dados.put(CarrinhoInterfaceContract.COL_TITLENOME, produtosItem.getNome());
        dados.put(CarrinhoInterfaceContract.COL_DESCRIC, produtosItem.getDescricao());
        dados.put(CarrinhoInterfaceContract.COL_PRECO, produtosItem.getPreco());
        dados.put(CarrinhoInterfaceContract.COL_IMAGE, produtosItem.getFoto());
        dados.put(CarrinhoInterfaceContract.COL_TOTAL_COMPRA, produtosItem.getTotal_Compra());
        dados.put(CarrinhoInterfaceContract.COL_DETALHE_PEDIDO, produtosItem.getDetalhe_Pedido());
        long id = db.insert(CarrinhoInterfaceContract.TABLE_PRODUCT,null,dados);

        if (id != -1) {
                produtosItem.getId() ;
            }


        db.close();

        return id;
    }

    public int atualizar(ProdutosItem produtosItem){

        acumValorTotal -= Double.parseDouble(produtosItem.getPreco());
        SQLiteDatabase db = carrinhoSQLHelper.getWritableDatabase();

        ContentValues dados = new ContentValues();

        dados.put(CarrinhoInterfaceContract.COL_TITLENOME, produtosItem.getNome());
        dados.put(CarrinhoInterfaceContract.COL_DESCRIC, produtosItem.getDescricao());
        dados.put(CarrinhoInterfaceContract.COL_PRECO, produtosItem.getPreco());
        dados.put(CarrinhoInterfaceContract.COL_IMAGE, produtosItem.getFoto());
        dados.put(CarrinhoInterfaceContract.COL_TOTAL_COMPRA, produtosItem.getTotal_Compra());
        dados.put(CarrinhoInterfaceContract.COL_DETALHE_PEDIDO, produtosItem.getDetalhe_Pedido());

        int linhasAfetadas = db.update( CarrinhoInterfaceContract.TABLE_PRODUCT, dados,
                CarrinhoInterfaceContract.COL_PRODUCT_ID +" =?",
                new String[]{String.valueOf(produtosItem.getId())});
        db.close();
        return linhasAfetadas;
    }

    public void excluir(ProdutosItem modelFavoriteProduct){

        SQLiteDatabase db = carrinhoSQLHelper.getWritableDatabase();


        db.delete(CarrinhoInterfaceContract.TABLE_PRODUCT, CarrinhoInterfaceContract.COL_PRODUCT_ID +" =?",
                new String[]{String.valueOf(modelFavoriteProduct.getId())});

        db.close();
    }

    public List<ProdutosItem> getProdutos() {
        List<ProdutosItem> modelFavoriteProductList = new ArrayList<>();
        SQLiteDatabase db = carrinhoSQLHelper.getReadableDatabase();


            Cursor cursorProdutos = db.rawQuery("SELECT * FROM " + CarrinhoInterfaceContract.TABLE_PRODUCT + " ORDER BY " +
                    CarrinhoInterfaceContract.COL_PRODUCT_ID, null);
            while (cursorProdutos.moveToNext()) {
                ProdutosItem modelFavoriteProduct = new ProdutosItem();

                modelFavoriteProduct.setId(Integer.parseInt(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_PRODUCT_ID))));

                modelFavoriteProduct.setNome(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_TITLENOME)));

                modelFavoriteProduct.setDescricao(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_DESCRIC)));

                modelFavoriteProduct.setPreco(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_PRECO)));

                modelFavoriteProduct.setFoto(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_IMAGE)));

                modelFavoriteProduct.setTotal_Compra(String.valueOf(acumValorTotal));

                modelFavoriteProduct.setDetalhe_Pedido(cursorProdutos.getString(
                        cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_DETALHE_PEDIDO)));

                modelFavoriteProductList.add(modelFavoriteProduct);

            }
        cursorProdutos.close();
        db.close();

        return modelFavoriteProductList;

    }

    public List<ProdutosItem> getPedidoFinal() {
        List<ProdutosItem> modelFavoriteProductList = new ArrayList<>();
        SQLiteDatabase db = carrinhoSQLHelper.getReadableDatabase();


        Cursor cursorProdutos = db.rawQuery("SELECT * FROM " + CarrinhoInterfaceContract.TABLE_PRODUCT + " ORDER BY " +
                CarrinhoInterfaceContract.COL_PRODUCT_ID, null);
        while (cursorProdutos.moveToNext()) {
            ProdutosItem modelFavoriteProduct = new ProdutosItem();

            modelFavoriteProduct.setId(Integer.parseInt(cursorProdutos.getString(
                    cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_PRODUCT_ID))));

            modelFavoriteProduct.setNome(cursorProdutos.getString(
                    cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_TITLENOME)));


            modelFavoriteProduct.setPreco(cursorProdutos.getString(
                    cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_PRECO)));


            modelFavoriteProduct.setDetalhe_Pedido(cursorProdutos.getString(
                    cursorProdutos.getColumnIndex(CarrinhoInterfaceContract.COL_DETALHE_PEDIDO)));

            modelFavoriteProductList.add(modelFavoriteProduct);

        }
        cursorProdutos.close();
        db.close();

        return modelFavoriteProductList;

    }



    public String getValorTotal(){
        if (acumValorTotal <= 0) {

            totalFechaCompra = 0;
        }

        ProdutosItem modelFavoriteProduct = new ProdutosItem();
        if (acumValorTotal > 0) {
            totalFechaCompra = acumValorTotal;
        }
        return String.valueOf(totalFechaCompra);

    }

    public boolean favoritar(ProdutosItem modelFavoriteProduct){
        boolean existe;
        SQLiteDatabase db = carrinhoSQLHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT "+ CarrinhoInterfaceContract.COL_PRODUCT_ID +
                        " FROM "+ CarrinhoInterfaceContract.TABLE_PRODUCT+
                        " WHERE "+ CarrinhoInterfaceContract.COL_TITLENOME+" = ?",
                new String[]{modelFavoriteProduct.getNome()});
        existe = c.getCount()> 0;
        db.close();
        return existe;
    }
/*
    public double getPreco(){

        SQLiteDatabase db = carrinhoSQLHelper.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + CarrinhoInterfaceContract.TABLE_PRODUCT, null);
        while (c.moveToNext()){
            totalFechaCompra = c.getDouble(c.getColumnIndex(CarrinhoInterfaceContract.COL_TOTAL_COMPRA));
        }
        c.close();
        return totalFechaCompra;
    }
    */
}
