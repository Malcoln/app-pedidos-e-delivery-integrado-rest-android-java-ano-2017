package aplicativo.tech.saborbrasilrestaurante;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import aplicativo.tech.saborbrasilrestaurante.controller.CarrinhoAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;


public class AlertCarrinho extends Application {

    CarrinhoAdapter carrinhoAdapter;

    CarrinhoDBRepo carrinhoDBRepo;

    Context context;

    TextView empty;

    SessionManagerPreConfig sessionManagerPreConfig;

    public AlertCarrinho(Context context) {
        this.context = context;
    }

    public void ExecCarrinho() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.listview_pedidos,null);

        empty = view.findViewById(R.id.empty);
        empty.setVisibility(View.GONE);

        sessionManagerPreConfig = new SessionManagerPreConfig(context);

        carrinhoDBRepo = new CarrinhoDBRepo(context);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setTitle("Itens do pedido");
        alertDialogBuilder.setIcon(R.drawable.botao_carrinho32);

        LinearLayout lineartotalPedido = view.findViewById(R.id.linearTotalPedido);
        lineartotalPedido.setVisibility(view.getVisibility());
        TextView strTotalPedido = view.findViewById(R.id.txtValor);
        strTotalPedido.setText("R$"+carrinhoDBRepo.getValorTotal()+"0");


        ListView listView = (ListView)view.findViewById(R.id.listPedidos);

        //VERIFICA VALOR DE CARRINHO E QUANTIDADE DE PRODUTO E INSTANCIA LISTA COM MENSAGEM DE VAZIA
        if (!carrinhoDBRepo.getValorTotal().equals("0.0")) {
            carrinhoAdapter = new CarrinhoAdapter(context, carrinhoDBRepo.getProdutos());
            listView.setAdapter(carrinhoAdapter);
        }
        else if (carrinhoDBRepo.getValorTotal().equals("0.0")){
            ProdutosItem produtosItem = new ProdutosItem();
            for (int k=0; k<carrinhoDBRepo.getProdutos().size();k++){

                carrinhoDBRepo.excluir(produtosItem);
            }
            empty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        context.startActivity(new Intent(context, MainActivity.class));
                    }
                })
                .setNegativeButton("Concluir Pedido", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean status_Login = sessionManagerPreConfig.isLoggedIn();
                        if (status_Login==true){
                            Intent it = new Intent(context, FechamentoPedidoActivity.class);
                            context.startActivity(it);
                        } else {
                            Toast.makeText(context,"Caro cliente favor efetuar Login em: Meu Perfil para a conclusão de seu pedido.", Toast.LENGTH_LONG).show();
                            Intent it = new Intent(context, MainActivity.class);
                            context.startActivity(it);
                        }

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
