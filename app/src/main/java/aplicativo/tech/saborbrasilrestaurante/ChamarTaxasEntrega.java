package aplicativo.tech.saborbrasilrestaurante;



import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonIOException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import aplicativo.tech.saborbrasilrestaurante.model.Taxas;


/**
 * Created by Malcoln on 22/03/2018.
 */

public class ChamarTaxasEntrega {


    static List<Taxas> mTaxasList;
    static Taxas taxas;


    public static void listarTaxas() {
        DownLoadTaxas downLoadTaxas = new DownLoadTaxas();
        downLoadTaxas.execute();
    }

    public static List<Taxas> obterTaxasServidor(){

        List<Taxas> taxasList = new ArrayList<>();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(5,TimeUnit.SECONDS);
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(AppConfig.BASE_URL + "api_bairros.php")
                .build();
        try{
            Response response = client.newCall(request).execute();

            String jsonStr = response.body().string();
            Log.e("SAIDA", "Resposta da URL: " + jsonStr);

            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONArray jsonArray=jsonObject.getJSONArray("data");

                for (int i=0; i< jsonArray.length();i++){
                    JSONObject jsonObjData = jsonArray.getJSONObject(i);
                    taxas = new Taxas();

                    taxas.setNome(jsonObjData.getString("nome"));
                    taxas.setTaxa(jsonObjData.getString("taxa"));


                    taxasList.add(taxas);

                }

            } catch (JSONException jex){
                jex.printStackTrace();
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        mTaxasList = taxasList;
        Log.e("TESTE LISTA", String.valueOf(taxasList));
        return taxasList;
    }
    private static class DownLoadTaxas extends AsyncTask<Void,Void, List<Taxas> >{

        @Override
        protected List<Taxas> doInBackground(Void... voids) {
            return ChamarTaxasEntrega.obterTaxasServidor();
        }
        @Override
        protected void onPostExecute(List<Taxas> taxas) {
            super.onPostExecute(taxas);
            mTaxasList = taxas;
            Log.e("TESTE 3: ", String.valueOf(mTaxasList));

        }

    }
}
