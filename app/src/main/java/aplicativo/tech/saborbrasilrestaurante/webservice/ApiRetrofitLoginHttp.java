package aplicativo.tech.saborbrasilrestaurante.webservice;


import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Malcoln on 15/03/2018.
 */

public interface ApiRetrofitLoginHttp {

    @GET("new_register.php")
    Call<UserRetrofit> performRegistration(
            @Query("name") String Name,
            @Query("user_email") String UserName,
            @Query("user_password") String UserPassword,
            @Query("user_endereco") String UserEndereco,
            @Query("user_bairro") String UserBairro,
            @Query("user_telefone") String UserTelefone,
            @Query("user_referencia") String UserReferencia);

    @GET("new_login.php")
    Call<UserRetrofit> performUserLogin(
            @Query("user_name") String UserName,
            @Query("user_password") String UserPassword);


}

