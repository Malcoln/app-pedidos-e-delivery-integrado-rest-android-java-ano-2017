package aplicativo.tech.saborbrasilrestaurante.webservice;

import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRetrofitPedido {

    @GET ("api_pedido.php")
    Call<UserRetrofit> performPedido(@Query("total_compra") String totalCompra,
                                     @Query("user_email") String email,
                                     @Query("detalhes_pedido") String detalhes_pedido,
                                     @Query("detalhes_pagamento") String detalhes_pagamento,
                                     @Query("nome") String nome,
                                     @Query("detalhes_entrega") String detalhes_entrega,
                                     @Query("det_bairro") String det_bairro,
                                     @Query("user_referencia") String user_referencia,
                                     @Query("det_status") String det_status,
                                     @Query("user_telefone") String user_telefone);
}
