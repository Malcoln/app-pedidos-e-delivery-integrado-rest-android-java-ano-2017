package aplicativo.tech.saborbrasilrestaurante.webservice;

import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRetrofitRecuperaSenha {
    @GET("recupera_senha.php")
    Call <UserRetrofit> performRecuperaSenha(
        @Query("user_email") String user_email,
        @Query("user_telefone") String user_telefone
    );
}
