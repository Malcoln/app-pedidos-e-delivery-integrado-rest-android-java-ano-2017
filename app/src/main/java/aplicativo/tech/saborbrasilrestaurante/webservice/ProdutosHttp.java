package aplicativo.tech.saborbrasilrestaurante.webservice;


import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import aplicativo.tech.saborbrasilrestaurante.model.CardapioDoDiaModel;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;


public class ProdutosHttp {
    public static final String BASE_URL = "http://saborbrasilrestaurante.com.br/webserviceprodutos.php";
    public static final String CARDAPIO_URL ="http://saborbrasilrestaurante.com.br/webserviceopcionais.php";

    public static List<ProdutosItem> obterProdutosJson(){

        List<ProdutosItem> listProdutosItem = new ArrayList<>();

        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(5, TimeUnit.SECONDS);
        client.setConnectTimeout(10,TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(BASE_URL)
                .build();
        try
        {
            Response response = client.newCall(request).execute();
            String jsonStr = response.body().string();
            Log.e("SAIDA", "Resposta da URL: " + jsonStr);

            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONArray jsonArray=jsonObject.getJSONArray("data");

                for (int i=0; i< jsonArray.length();i++){
                    JSONObject jsonObjData = jsonArray.getJSONObject(i);
                    ProdutosItem produtosItem = new ProdutosItem();
                    produtosItem.setId(Integer.parseInt(jsonObjData.getString("id")));
                    produtosItem.setNome(jsonObjData.getString("nome"));
                    produtosItem.setDescricao(jsonObjData.getString("descricao"));
                    produtosItem.setFoto(jsonObjData.getString("foto"));
                    produtosItem.setCategoria(jsonObjData.getString("categoria"));
                    produtosItem.setPreco(jsonObjData.getString("preco"));

                    listProdutosItem.add(produtosItem);
                }

            } catch (JSONException jex){
                jex.printStackTrace();
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }


        return listProdutosItem;
    }

    public static CardapioDoDiaModel obterCardapioDoDia(){

        CardapioDoDiaModel cardapioDoDiaModel = new CardapioDoDiaModel();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(5, TimeUnit.SECONDS);
        client.setConnectTimeout(10,TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(CARDAPIO_URL)
                .build();
        try
        {
            Response response = client.newCall(request).execute();
            String jsonStr = response.body().string();
            Log.e("SAIDA", "CARDAPIO: " + jsonStr);

            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONArray jsonArray=jsonObject.getJSONArray("data");

                for (int i=jsonArray.length()-1; i< jsonArray.length();i++){
                    JSONObject jsonObjData = jsonArray.getJSONObject(i);
                    CardapioDoDiaModel produtosItem = new CardapioDoDiaModel();
                    produtosItem.setId(jsonObjData.getString("id"));
                    produtosItem.setOpcao1(jsonObjData.getString("opcao1"));
                    produtosItem.setOpcao2(jsonObjData.getString("opcao2"));
                    produtosItem.setOpcao3(jsonObjData.getString("opcao3"));
                    produtosItem.setOpcao4(jsonObjData.getString("opcao4"));
                    produtosItem.setOpcao5(jsonObjData.getString("opcao5"));
                    produtosItem.setOpcao6(jsonObjData.getString("opcao6"));
                    produtosItem.setOpcao7(jsonObjData.getString("opcao7"));
                    produtosItem.setOpcao8(jsonObjData.getString("opcao8"));
                    produtosItem.setOpcao9(jsonObjData.getString("opcao9"));
                    produtosItem.setOpcao10(jsonObjData.getString("opcao10"));
                    produtosItem.setOpcao11(jsonObjData.getString("opcao11"));
                    produtosItem.setOpcao12(jsonObjData.getString("opcao12"));
                    produtosItem.setOpcao13(jsonObjData.getString("opcao13"));
                    produtosItem.setOpcao14(jsonObjData.getString("opcao14"));
                    produtosItem.setOpcao15(jsonObjData.getString("opcao15"));
                    produtosItem.setOpcao16(jsonObjData.getString("opcao16"));


                    cardapioDoDiaModel = produtosItem;
                }

            } catch (JSONException jex){
                jex.printStackTrace();
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        Log.e("SAIDA", "JSON CARDAPIO DO DIA:"+cardapioDoDiaModel);
        return cardapioDoDiaModel;
    }

    public static List<CardapioDoDiaModel> obterListaCardapioDoDia(){

        List<CardapioDoDiaModel> cardapioDoDiaModel = new ArrayList<>();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(5, TimeUnit.SECONDS);
        client.setConnectTimeout(10,TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(CARDAPIO_URL)
                .build();
        try
        {
            Response response = client.newCall(request).execute();
            String jsonStr = response.body().string();
            Log.e("SAIDA", "CARDAPIO: " + jsonStr);

            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONArray jsonArray=jsonObject.getJSONArray("data");

                for (int i=jsonArray.length()-1; i< jsonArray.length();i++){
                    JSONObject jsonObjData = jsonArray.getJSONObject(i);
                    CardapioDoDiaModel produtosItem = new CardapioDoDiaModel();
                    produtosItem.setId(jsonObjData.getString("id"));
                    produtosItem.setOpcao1(jsonObjData.getString("opcao1"));
                    produtosItem.setOpcao2(jsonObjData.getString("opcao2"));
                    produtosItem.setOpcao3(jsonObjData.getString("opcao3"));
                    produtosItem.setOpcao4(jsonObjData.getString("opcao4"));
                    produtosItem.setOpcao5(jsonObjData.getString("opcao5"));
                    produtosItem.setOpcao6(jsonObjData.getString("opcao6"));
                    produtosItem.setOpcao7(jsonObjData.getString("opcao7"));
                    produtosItem.setOpcao8(jsonObjData.getString("opcao8"));
                    produtosItem.setOpcao9(jsonObjData.getString("opcao9"));
                    produtosItem.setOpcao10(jsonObjData.getString("opcao10"));
                    produtosItem.setOpcao11(jsonObjData.getString("opcao11"));
                    produtosItem.setOpcao12(jsonObjData.getString("opcao12"));
                    produtosItem.setOpcao13(jsonObjData.getString("opcao13"));
                    produtosItem.setOpcao14(jsonObjData.getString("opcao14"));
                    produtosItem.setOpcao15(jsonObjData.getString("opcao15"));
                    produtosItem.setOpcao16(jsonObjData.getString("opcao16"));


                    cardapioDoDiaModel.add(produtosItem);
                }

            } catch (JSONException jex){
                jex.printStackTrace();
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        Log.e("SAIDA", "JSON CARDAPIO DO DIA:"+cardapioDoDiaModel);
        return cardapioDoDiaModel;
    }
}