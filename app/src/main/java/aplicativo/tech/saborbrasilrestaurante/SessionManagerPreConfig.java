package aplicativo.tech.saborbrasilrestaurante;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Malcoln on 15/03/2018.
 */

public class SessionManagerPreConfig {
    // LogCat tag
    private static String TAG = SessionManagerPreConfig.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences sharedPreferences;

    private Context context;

    SharedPreferences.Editor editor;

    // Shared sharedPreferences mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private String PREF_NAME ;



    public SessionManagerPreConfig(Context context) {
        this.context = context;
        PREF_NAME = context.getString(R.string.pref_file);
        sharedPreferences = this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

    }

    public void setLoginStatus(boolean loginStatus) {
        editor = sharedPreferences.edit();
        editor.putBoolean(context.getString(R.string.pref_login_status), loginStatus);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){

        return sharedPreferences.getBoolean(context.getString(R.string.pref_login_status), false);
    }

    public void setWriteName(String name){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_name), name);
        editor.commit();
    }

    public void setDataUser(String endereco, String bairro, String taxa_bairro, String telefone, String email, String ponto_referencia){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_user_endereco), endereco);
        editor.putString(context.getString(R.string.pref_user_bairro), bairro);
        editor.putString(context.getString(R.string.pref_user_taxa_bairro), taxa_bairro);
        editor.putString(context.getString(R.string.pref_user_telefone), telefone);
        editor.putString(context.getString(R.string.pref_user_email), email);
        editor.putString(context.getString(R.string.pref_user_referencia), ponto_referencia);
        editor.commit();
    }

    public String readNameFromUser(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_name), "seu nome");
    }

    public String getEnderecoFromUser(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_endereco), "endereço");
    }

    public String getBairro(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_bairro), "bairro");
    }

    public String getTaxaBairro(){
        return  sharedPreferences.getString(context.getString(R.string.pref_user_taxa_bairro), "taxa entrega");
    }

    public String getTelefone(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_telefone), "telefone");
    }

    public String getReferencia(){
        return sharedPreferences.getString(context.getString(R.string.pref_user_referencia), "ponto referencia");
    }
    public void displayToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public String getEmail() {
        return sharedPreferences.getString(context.getString(R.string.pref_user_email),"email");
    }
}
