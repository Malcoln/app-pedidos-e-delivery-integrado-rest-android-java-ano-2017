package aplicativo.tech.saborbrasilrestaurante;


import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import aplicativo.tech.saborbrasilrestaurante.db_persistencia.UserSQLiteHandler;
import aplicativo.tech.saborbrasilrestaurante.model.UserRetrofit;
import aplicativo.tech.saborbrasilrestaurante.webservice.ApiRetrofitLoginHttp;
import aplicativo.tech.saborbrasilrestaurante.webservice.ApiRetrofitRecuperaSenha;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginFragment extends Fragment {

    @BindView(R.id.appBar)
    AppBarLayout mAppBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.email)
    EditText inputEmail;

    @BindView(R.id.password)
    EditText inputPassword;

    @BindView(R.id.telefone)
    EditText inputTelefone;

    @BindView(R.id.btnLoginOficial)
    Button btnLogin;

    @BindView(R.id.btnLinkToRegisterScreen)
    Button btnLinkToCadastro;

    @BindView(R.id.userNameEsqueciAccountId)
    Button btnEsqueciLogin;

    private String email ;
    private String password;

    //LoginTask loginTask;

    private ProgressDialog pDialog;

    private SessionManagerPreConfig session;

    private UserSQLiteHandler db;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_login, container, false);

        ButterKnife.bind(this,view);

        inputTelefone.setVisibility(View.GONE);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        final ActionBar ab = (ActionBar)((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new UserSQLiteHandler(getActivity());

        // Session Manager to SharedPreferences
        session = new SessionManagerPreConfig(getActivity());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // UserRetrofit is already logged in. Take him to USER PERFIL ACTIVITY or PERFIL FRAGMENT

            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new PerfilFragment()).commit();
            //finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (inputEmail.getText().toString().isEmpty() || inputPassword.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Favor preencher os campos corretamente",Toast.LENGTH_LONG).show();
                } else {
                    performLoggin();
                }

            }

        });

        String registro = (btnLinkToCadastro.getText().toString());
        SpannableString spannAbleString = new SpannableString(registro);
        spannAbleString.setSpan(new UnderlineSpan(), 0,registro.length(), 0);
        btnLinkToCadastro.setText(spannAbleString);

        // Link to Register Screen
        btnLinkToCadastro.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Fragment fragment = new RegisterActivity();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment,null).commit();

            }
        });

        btnEsqueciLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = inputEmail.getText().toString();
                inputTelefone.setVisibility(View.VISIBLE);
                inputPassword.setVisibility(View.GONE);
                btnLogin.setVisibility(View.GONE);
                btnLinkToCadastro.setVisibility(View.GONE);

                btnEsqueciLogin.setText("ENVIAR");
                if (!inputEmail.getText().toString().isEmpty() && !inputTelefone.getText().toString().isEmpty()){
                    performRecuperaSenha();
                } else {
                    Toast.makeText(getActivity(),"Favor preencher os campos corretamente.", Toast.LENGTH_LONG).show();
                }

            }
        });

        return view;
    }

    private ApiRetrofitLoginHttp getApiLogginHttp(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ApiRetrofitLoginHttp apiRetrofitLoginHttp = retrofit.create(ApiRetrofitLoginHttp.class);
        return apiRetrofitLoginHttp;
    }

    private void performLoggin() {
        final String username = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();

        ApiRetrofitLoginHttp apiRetrofitLoginHttp = this.getApiLogginHttp();
        Call<UserRetrofit> userCall = apiRetrofitLoginHttp.performUserLogin(username,password);
        userCall.enqueue(new Callback<UserRetrofit>() {
            @Override
            public void onResponse(Call<UserRetrofit> call, Response<UserRetrofit> response) {
                if (response.body().getResponse().equals("ok")){

                    //SHAREDPREFERENCES
                    session.setLoginStatus(true);
                    session.setWriteName(response.body().getName());
                    session.setDataUser(response.body().getEndereco(), response.body().getBairro(),
                            null,response.body().getTelefone(),response.body().getLogin(),response.body().getReferencia());

                    Toast.makeText(getActivity(), "Login efetuado com sucesso", Toast.LENGTH_SHORT).show();

                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, new PerfilFragment()).commit();

                } else if (response.body().getResponse().equals("failed")){
                    Toast.makeText(getActivity(), "Usuário não registrado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserRetrofit> call, Throwable t) {

            }
        });
    }

    private ApiRetrofitRecuperaSenha getApiRetrofitRecuperaSenha(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiRetrofitRecuperaSenha apiRetrofitRecuperaSenha = retrofit.create(ApiRetrofitRecuperaSenha.class);
        return apiRetrofitRecuperaSenha;
    }

    private void performRecuperaSenha(){
        final String username = inputEmail.getText().toString().trim();
        final String userTelefone = inputTelefone.getText().toString().trim();

        ApiRetrofitRecuperaSenha apiRetrofitRecuperaSenha = this.getApiRetrofitRecuperaSenha();
        Call<UserRetrofit> userCall = apiRetrofitRecuperaSenha.performRecuperaSenha(username,userTelefone);
        userCall.enqueue(new Callback<UserRetrofit>() {
            @Override
            public void onResponse(Call<UserRetrofit> call, Response<UserRetrofit> response) {
                if (response.body().getResponse().equals("ok")){
                    String senha = response.body().getSenha();
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Recuperação de Senha:");
                    alertDialog.setMessage("Senha: "+senha);
                    alertDialog.setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Fragment fragment = new LoginFragment();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_container,fragment);
                            fragmentTransaction.commit();
                        }
                    });
                    AlertDialog alertDialogo = alertDialog.create();
                    alertDialogo.show();

                    //Uri uri = Uri.parse("smsto:" + "");
                    //Intent sendSenha = new Intent(Intent.ACTION_VIEW, uri);
                    //sendSenha.putExtra("sms_body", "Senha: "+senha)
                            ;


                    //Toast.makeText(getActivity(), "Seus dados serão enviados por SMS.", Toast.LENGTH_SHORT).show();



                } else if (response.body().getResponse().equals("failed")){
                    Toast.makeText(getActivity(), "Usuário não registrado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserRetrofit> call, Throwable t) {

            }
        });
    }

}
