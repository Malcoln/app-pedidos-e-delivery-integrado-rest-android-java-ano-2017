/*
 * Copyright (c) $today.year.SpheraSystems I.T - 2017
 */

package aplicativo.tech.saborbrasilrestaurante;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import aplicativo.tech.saborbrasilrestaurante.controller.CarrinhoAdapter;
import aplicativo.tech.saborbrasilrestaurante.controller.ProdutosListAdapter;
import aplicativo.tech.saborbrasilrestaurante.controller.TaxasAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import aplicativo.tech.saborbrasilrestaurante.model.Taxas;
import aplicativo.tech.saborbrasilrestaurante.webservice.ProdutosHttp;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Malcoln - SpheraSystems I.T
 */
public class ProdutosListWebServiceFragment extends Fragment
        implements ProdutosListAdapter.AoClicarNaPromocaoListener{

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout mSwipe;

    @BindView(R.id.recyclerViewPedido)
    RecyclerView mRecyclerView;

    @BindView(R.id.coordinator)
    CoordinatorLayout mCoordinator;

    @BindView(R.id.appBar)
    AppBarLayout mAppBar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.mSearchView)
    EditText txtSearch;

    @BindView(R.id.btnMarmitex)
    Button btnMarmitex;

    @BindView(R.id.btnSobremesas)
    Button btnSobremesas;

    @BindView(R.id.btnReceitas)
    Button btnReceitas;

    @BindView(R.id.btnSucos)
    Button btnSucos;

    @BindView(R.id.btnBebidas)
    Button btnBebidas;

    @BindView(R.id.btnTodos)
    Button btnTodos;

    @BindView(R.id.mFabCarrinho)
    FloatingActionButton fabCarrinho;

    private List<ProdutosItem> mProdutosItem;
    ProdutosDownloadTask mTask;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_activity_main,container,false);

        ButterKnife.bind(this,view);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        toolbar.setTitle("");

        final ActionBar ab = (ActionBar)((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mTask = new ProdutosDownloadTask();
                mTask.execute();
            }
        });

        mRecyclerView.setTag("web");
        mRecyclerView.setHasFixedSize(true);
        //if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT){
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //} else {
           // mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
       // }

        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAppBar.setExpanded(false);
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                mAppBar.setExpanded(true);

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newText = txtSearch.getText().toString();
                buscar(newText);
                mAppBar.setExpanded(false);
            }
            @Override
            public void afterTextChanged(Editable editable) {  }
        });

        /************ TRATAMENTO CLICK NOS BOTÕES DE FILTROS POR CATEGORIA
         * POR PADRÃO BOTÃO ´todos´ INICIA VERMELHO
         **********************************************************************/
        btnTodos.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btnTodos.setTextColor(Color.parseColor("#ffffff"));

        btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //btnTodos.setCardBackgroundColor(getResources().getColor(R.color.colorAviso));
                btnTodos.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnTodos.setTextColor(Color.parseColor("#ffffff"));
                setMarmitexBackground();
                setBebidasBackground();
                setReceitasBackground();
                setSobremesasBackground();
                setSucosBackground();
                mTask = new ProdutosDownloadTask();
                mTask.execute();
            }
        });

        btnMarmitex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnMarmitex.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnMarmitex.setTextColor(Color.parseColor("#ffffff"));
                setTodasCategoriasBackground();
                setBebidasBackground();
                setReceitasBackground();
                setSobremesasBackground();
                setSucosBackground();
                buscarClick("marmitex");
            }
        });

        btnSobremesas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSobremesas.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnSobremesas.setTextColor(Color.parseColor("#ffffff"));
                setTodasCategoriasBackground();
                setBebidasBackground();
                setReceitasBackground();
                setMarmitexBackground();
                setSucosBackground();
                buscarClick("sobremesa");
            }
        });

        btnReceitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnReceitas.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnReceitas.setTextColor(Color.parseColor("#ffffff"));
                setTodasCategoriasBackground();
                setBebidasBackground();
                setSobremesasBackground();
                setMarmitexBackground();
                setSucosBackground();
                buscarClick("receitas");
            }
        });
        btnSucos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSucos.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnSucos.setTextColor(Color.parseColor("#ffffff"));
                setTodasCategoriasBackground();
                setBebidasBackground();
                setReceitasBackground();
                setMarmitexBackground();
                setSobremesasBackground();
                buscarClick("suco");
            }
        });
        btnBebidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnBebidas.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                btnBebidas.setTextColor(Color.parseColor("#ffffff"));
                setTodasCategoriasBackground();
                setSobremesasBackground();
                setReceitasBackground();
                setMarmitexBackground();
                setSucosBackground();
                buscarClick("refrigerante");
            }
        });

        fabCarrinho.setVisibility(View.INVISIBLE);

        mostraBotaoFAB();

        fabCarrinho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertCarrinho alertCarrinho = new AlertCarrinho(getActivity());
                alertCarrinho.ExecCarrinho();

            }
        });


        //searchView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        return view;
    }

    private void setTodasCategoriasBackground() {
        btnTodos.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnTodos.setTextColor(Color.parseColor("#000000"));
    }

    private void setSucosBackground() {
        btnSucos.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnSucos.setTextColor(Color.parseColor("#000000"));
    }

    private void setSobremesasBackground() {
        btnSobremesas.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnSobremesas.setTextColor(Color.parseColor("#000000"));
    }

    private void setReceitasBackground() {
        btnReceitas.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnReceitas.setTextColor(Color.parseColor("#000000"));
    }

    private void setBebidasBackground() {
        btnBebidas.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnBebidas.setTextColor(Color.parseColor("#000000"));
    }

    private void setMarmitexBackground() {
        btnMarmitex.setBackgroundColor(getResources().getColor(R.color.colorLightMalcoln));
        btnMarmitex.setTextColor(Color.parseColor("#000000"));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //chamar unbind da view
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mProdutosItem == null){
            if (mTask == null){
                mTask = new ProdutosDownloadTask();
                mTask.execute();
            } else if (mTask.getStatus() == AsyncTask.Status.RUNNING){
                exibirProgresso();
            }
        } else {
            atualizarLista();
        }
        limparBusca();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTask!=null)mTask.cancel(true);
    }

    @Override
    public void aoClicarNoItemNovidades(View view, int position, ProdutosItem novidadesItem) {
        Intent it = new Intent(getActivity(), ProdutosDetalhesActivity.class);
        it.putExtra(ProdutosDetalhesActivity.EXTRA_ITEM, novidadesItem);
        startActivity(it);
    }

    private void atualizarLista() {
        ProdutosListAdapter adapter = new ProdutosListAdapter(getActivity(), mProdutosItem);
        adapter.setAoCLicarNaPromocaoListener(this);
        mRecyclerView.setAdapter(adapter);
    }

    public void buscar(String src){
        if (src==null || src.trim().equals("")){
            atualizarLista();
            return;
        }
        List<ProdutosItem> listEcontrada =  new ArrayList<>(mProdutosItem);

        for (int i= listEcontrada.size()-1;i>=0;i-- ) {
            ProdutosItem newProd = listEcontrada.get(i);
            String text = newProd.getNome().toUpperCase();

            if (!text.contains(src.toUpperCase())){
               listEcontrada.remove(newProd);
            }
        }
        ProdutosListAdapter adapter = new ProdutosListAdapter(getActivity(), listEcontrada);
        adapter.setAoCLicarNaPromocaoListener(this);
        mRecyclerView.setAdapter(adapter);
    }
    private void limparBusca() {
        atualizarLista();
    }

    public void buscarClick(String src){
        List<ProdutosItem> listEcontrada =  new ArrayList<>(mProdutosItem);

        for (int i= listEcontrada.size()-1;i>=0;i-- ) {
            ProdutosItem newProd = listEcontrada.get(i);
            String text = newProd.getCategoria().toUpperCase();

            if (!text.contains(src.toUpperCase())){

                listEcontrada.remove(newProd);
            }
        }
        ProdutosListAdapter adapter = new ProdutosListAdapter(getActivity(), listEcontrada);
        adapter.setAoCLicarNaPromocaoListener(this);
        mRecyclerView.setAdapter(adapter);
    }

    public void exibirProgresso(){
        mSwipe.post(new Runnable() {
            @Override
            public void run() {
                mSwipe.setRefreshing(true);                
            }
        });
    }


    private class ProdutosDownloadTask extends AsyncTask<Void, Void, List<ProdutosItem>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            exibirProgresso();
        }

        @Override
        protected List<ProdutosItem> doInBackground(Void... voids) {

            return ProdutosHttp.obterProdutosJson();
        }

        @Override
        protected void onPostExecute(List<ProdutosItem> produtosItems) {
            super.onPostExecute(produtosItems);
            mSwipe.setRefreshing(false);
            if (produtosItems!= null){
                mProdutosItem = produtosItems;
                atualizarLista();
            }
        }
    }


    private void mostraBotaoFAB() {
        CarrinhoDBRepo carrinhoDBRepo = new CarrinhoDBRepo(getActivity());
        int contadorProdutos = carrinhoDBRepo.getProdutos().size();
        if (contadorProdutos>0){
            fabCarrinho.setVisibility(View.VISIBLE);
        } else {
            fabCarrinho.setVisibility(View.INVISIBLE);
        }
    }
}
