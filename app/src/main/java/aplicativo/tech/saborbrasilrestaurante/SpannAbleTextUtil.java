package aplicativo.tech.saborbrasilrestaurante;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;

public class SpannAbleTextUtil extends Application{

    Context context;

    private String saidaSublinhada;

    public SpannAbleTextUtil (Context context){
        this.context = context;
    }

    public String stringEstilizada(String string){
        SpannableString stringEstilizada = new SpannableString(
                string /*INDEX 43 - 51 */
        );

        stringEstilizada.setSpan(new StrikethroughSpan(),0,7, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        /* COLOCANDO UM PONTO / BULLET NA FRENTE DO TEXTO,
         * COMO EM LISTAS HTML
         * */
        stringEstilizada.setSpan(
                new BulletSpan(10),
                13,
                13,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE );

        /* COLOCANDO O TEXTO COMO SUBLINHADO */
        stringEstilizada.setSpan(
                new UnderlineSpan(),
                22,
                32,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE );
        /* COLOCANDO O TEXTO COMO ITÁLICO */
        stringEstilizada.setSpan(
                new StyleSpan(Typeface.ITALIC),
                34,
                41,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE );

        /* COLOCANDO O TEXTO COM UM TRAÇO NO MEIO */
        stringEstilizada.setSpan(
                new StrikethroughSpan(),
                43,
                51,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE );

        stringEstilizada.setSpan(
                new ForegroundColorSpan( Color.RED ),
                53,
                61,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE );

        stringEstilizada.setSpan(
                new BackgroundColorSpan(Color.BLUE),
                63,
                72,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE );
        return string;
    }

    public String stringDestacada(String string){

        SpannableString spannableString = new SpannableString(
                string
        );

        spannableString.setSpan(new BackgroundColorSpan(Color.BLUE),
                0,8,Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        String saida = String.valueOf(spannableString);

        return saida;
    }

    public String getSaidaSublinhada() {
        return saidaSublinhada;
    }


    public void setStringSublinhada(String string){
        SpannableString spannableString = new SpannableString(string);
        spannableString.setSpan(new StrikethroughSpan(), 0,8,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        this.saidaSublinhada = String.valueOf(spannableString);

    }
}
