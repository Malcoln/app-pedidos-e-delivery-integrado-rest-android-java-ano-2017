package aplicativo.tech.saborbrasilrestaurante;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import aplicativo.tech.saborbrasilrestaurante.controller.ProdutoDetalheAdapter;
import aplicativo.tech.saborbrasilrestaurante.db_persistencia.CarrinhoDBRepo;
import aplicativo.tech.saborbrasilrestaurante.model.CardapioDoDiaModel;
import aplicativo.tech.saborbrasilrestaurante.model.ProdutosItem;
import aplicativo.tech.saborbrasilrestaurante.webservice.ProdutosHttp;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ProdutosDetalhesActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "produtos";

    ////////////////////////////////////////////////////
    /////////////COMPONENTES E ATRIBUTOS REFERENTES A VIEWPAGER DA(S) IMAGENS WEBSERVICE
    private ViewPager viewPager;
    private ProdutoDetalheAdapter mProdutoViewPagAdpter;
    private LinearLayout dotsLayout;
    private ImageButton btnGo, btnReturn;
    private TextView[] dots;
    int[] layout = new int[]{R.layout.activity_inicial};

    ///////////////////////////////////////////////////

    int qtdeClick;

    // String Global para pegar detalhes do pedido na view
    private String strGlobal;

    // String Global para receber estado da strGlobal ou strings de CheckBox
    private StringBuilder saidaDetalhePedido;

    String[] strOpcaodeSuco;
    private boolean ehSuco;

    String[] strOpcaoMarmita;

    String[] strOpcaoBebida;


    @BindView(R.id.imgImovel)
    ImageView mImageImovel;

    //@BindView(R.id.recyclerViewCardapio)
    //RecyclerView mRecyclerView;

    @BindView(R.id.coordinator)
    CoordinatorLayout mCoordinator;

    AppBarLayout mAppBar;

    @BindView(R.id.collapseToolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.txtPrecoDet)
    TextView txtPreco;

    @BindView(R.id.strDescricao)
    TextView strDescricao;

    @BindView(R.id.strNome)
    TextView strNome;

    @BindView(R.id.strOpcao1)
    CheckBox strOpcao1;
    @BindView(R.id.strOpcao2)
    CheckBox strOpcao2;
    @BindView(R.id.strOpcao3)
    CheckBox strOpcao3;
    @BindView(R.id.strOpcao4)
    CheckBox strOpcao4;
    @BindView(R.id.strOpcao5)
    CheckBox strOpcao5;
    @BindView(R.id.strOpcao6)
    CheckBox strOpcao6;
    @BindView(R.id.strOpcao7)
    CheckBox strOpcao7;
    @BindView(R.id.strOpcao8)
    CheckBox strOpcao8;
    @BindView(R.id.strOpcao9)
    CheckBox strOpcao9;
    @BindView(R.id.strOpcao10)
    CheckBox strOpcao10;
    @BindView(R.id.strOpcao11)
    CheckBox strOpcao11;
    @BindView(R.id.strOpcao12)
    CheckBox strOpcao12;
    @BindView(R.id.strOpcao13)
    CheckBox strOpcao13;
    @BindView(R.id.strOpcao14)
    CheckBox strOpcao14;
    @BindView(R.id.strOpcao15)
    CheckBox strOpcao15;
    @BindView(R.id.strOpcao16)
    CheckBox strOpcao16;

    @BindView(R.id.btnMenos)
    ImageButton btnMenos;

    @BindView(R.id.btnMais)
    ImageButton btnMais;

    @BindView(R.id.txtQuant)
    TextView txtQtde;

    @BindView(R.id.txtQtde)
    TextView textQtde;

    @BindView(R.id.txtQtdeAviso)
    TextView textQtdeAviso;

    @BindView(R.id.btnCarrinhoAdd)
    Button btnCarrinhoADD;

    @BindView(R.id.strDetalhesPedido)
    EditText strDetalhesPedido;

    @BindView(R.id.cardViewTextoOpcoes)
    CardView cardViewTextOpcoes;

    @BindView(R.id.etxPedido)
    RelativeLayout etxPedido;

    private CardapioDoDiaModel mCardapioDoDiaModelList;

    ProdutosDownloadTask mTask;

    @BindView(R.id.cardapioDiario)
    RelativeLayout compCardapioDiario;


    //CONTAINER CHECKLIST PARA SUCOS
    @BindView(R.id.checklist_sucos)
    RelativeLayout checkListSucos;

    @BindView(R.id.chkGelo)
    CheckBox chkGelo;
    @BindView(R.id.chkSemGelo)
    CheckBox chkSemGelo;
    @BindView(R.id.chkAcucar)
    CheckBox chkAcucar;
    @BindView(R.id.chkAdocante)
    CheckBox chkAdocante;
    @BindView(R.id.chkNatural)
    CheckBox chkNatural;
    private boolean ehMarmita;

    //CONTAINER CHECKLIST BEBIDAS
    @BindView(R.id.checklist_bebidas)
    RelativeLayout checkListBebidas;

    @BindView(R.id.chkGelada)
    CheckBox chkGelada;
    @BindView(R.id.chkBebidaSemGelo)
    CheckBox chkBebidaSemGelo;

    private boolean ehBebida;
    private boolean ehReceita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos_detalhe);

        ButterKnife.bind(this);

        final ProdutosItem produtosItem = (ProdutosItem) getIntent().getSerializableExtra(EXTRA_ITEM);

        preencheCampos(produtosItem);

        configurarBarraDeTitulo(produtosItem.getNome());

        carregarCapa(produtosItem);

        //////////////////////////////////////////////////////////
        ///////// INSTANCIAS/OBJETOS REFERENTES A VIEWPAGER DAS IMAGENS DE WEBSERVICE
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        mProdutoViewPagAdpter = new ProdutoDetalheAdapter(this, layout, produtosItem.getFoto());

        viewPager.setAdapter(mProdutoViewPagAdpter);

        if (mCardapioDoDiaModelList == null){
            if (mTask == null){
                mTask = new ProdutosDownloadTask();
                mTask.execute();
            } else if (mTask.getStatus() == AsyncTask.Status.RUNNING){
                //exibirProgresso();
            }
        } else {
            //atualizarCardapio();
        }

       // atualizarCardapio();

        //limparBusca();


        if (produtosItem.getCategoria().equals("MARMITEX")){
            compCardapioDiario.setVisibility(View.VISIBLE);
            checkListSucos.setVisibility(View.GONE);
            checkListBebidas.setVisibility(View.GONE);
            capturaChkBoxMarmitas();
            ehMarmita=true;
            ehSuco=false;

        }

        if (produtosItem.getCategoria().equals("BOLOS, DOCES E SOBREMESAS")){
            compCardapioDiario.setVisibility(View.GONE);
            checkListSucos.setVisibility(View.GONE);
            checkListBebidas.setVisibility(View.GONE);
            cardViewTextOpcoes.setVisibility(View.GONE);
            etxPedido.setVisibility(View.GONE);
            ehMarmita=false;
            ehSuco=false;

        }

        if (produtosItem.getCategoria().equals("RECEITAS DO DIA - ITENS VENDIDOS SEPARADAMENTE")){
            compCardapioDiario.setVisibility(View.GONE);
            checkListSucos.setVisibility(View.GONE);
            checkListBebidas.setVisibility(View.GONE);
            cardViewTextOpcoes.setVisibility(View.GONE);
            etxPedido.setVisibility(View.VISIBLE);
            ehMarmita=false;
            ehSuco=false;
            ehBebida=false;
            ehReceita=true;
        }

        if (produtosItem.getCategoria().equals("SUCOS NATURAIS")){
            compCardapioDiario.setVisibility(View.GONE);
            checkListBebidas.setVisibility(View.GONE);
            ehMarmita=false;
            ehSuco=true;
            capturaChkBoxSuco();
        }

        if (produtosItem.getCategoria().equals("REFRIGERANTES E CERVEJAS")){
            compCardapioDiario.setVisibility(View.GONE);
            checkListSucos.setVisibility(View.GONE);
            cardViewTextOpcoes.setVisibility(View.GONE);

            ehMarmita=false;
            ehSuco=false;
            ehBebida=true;
            capturaChkBoxBebidas();
        }

        calculadoraCarrinho(produtosItem);

    }

    private void capturaChkBoxBebidas(){

        strOpcaoBebida = new String[2];
        for (int r=0; r<strOpcaoBebida.length; r++){
            strOpcaoBebida[r] = "";
        }

        chkGelada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strOpcaoBebida[0] = "gelado,";
                if (chkGelada.isChecked()){
                    chkBebidaSemGelo.setChecked(false);
                } else if (!chkGelada.isChecked()){
                    strOpcaoBebida[0] = "";
                }
            }
        });

        chkBebidaSemGelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strOpcaoBebida[1] = "sem gelo";
                if (chkBebidaSemGelo.isChecked()){
                    chkGelada.setChecked(false);
                } else if (!chkBebidaSemGelo.isChecked()){
                    strOpcaoBebida[1]="";
                }
            }
        });
    }

    //ABRE SUCOS
    private void capturaChkBoxSuco() {

        strOpcaodeSuco = new String[5];
        for (int w=0; w<strOpcaodeSuco.length; w++){
            strOpcaodeSuco[w] = "";
        }

        chkAcucar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaodeSuco[0] = "com Açucar,";

                if (chkAcucar.isChecked()){
                    chkAdocante.setChecked(false);
                } else if (!chkAcucar.isChecked()){
                    strOpcaodeSuco[0] = "";
                }
            }
        });
        chkAdocante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaodeSuco[1] = "com Adoçante,";
                if (chkAdocante.isChecked()){
                    chkAcucar.setChecked(false);
                } else if (!chkAdocante.isChecked()){
                    strOpcaodeSuco[1] = "";
                }
            }
        });

        chkGelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaodeSuco[2] = "com Gelo,";
                if (chkGelo.isChecked()){
                    chkSemGelo.setChecked(false);
                } else if (!chkGelo.isChecked()){
                    strOpcaodeSuco[2] = "";
                }
            }
        });

        chkSemGelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaodeSuco[3] = "sem Gelo,";
                if (chkSemGelo.isChecked()){
                    chkGelo.setChecked(false);
                } else if (!chkSemGelo.isChecked()){
                    strOpcaodeSuco[3] = "";
                }
            }
        });

        chkNatural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaodeSuco[4] = "natural";
                strOpcaodeSuco[3] = "";
                strOpcaodeSuco[1] = "";
                strOpcaodeSuco[1] = "";
                strOpcaodeSuco[0] = "";
                if (chkNatural.isChecked()){
                    chkAdocante.setChecked(false);
                    chkAcucar.setChecked(false);
                    chkSemGelo.setChecked(false);
                    chkGelo.setChecked(false);
                }
            }
        });

    }

    // FECHA SUCOS

    // ABRE MARMITAS
    private void capturaChkBoxMarmitas(){

        strOpcaoMarmita = new String[16];

        for (int w=0; w<strOpcaoMarmita.length; w++){
            strOpcaoMarmita[w] = "";
        }

        strOpcao1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[0] = mCardapioDoDiaModelList.getOpcao1()+",";
            }
        });
        strOpcao2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[1] = mCardapioDoDiaModelList.getOpcao2()+",";
            }
        });
        strOpcao3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[2] = mCardapioDoDiaModelList.getOpcao3()+",";
            }
        });
        strOpcao4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[3] = mCardapioDoDiaModelList.getOpcao4()+",";
            }
        });
        strOpcao5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[4] = mCardapioDoDiaModelList.getOpcao5()+",";
            }
        });
        strOpcao6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[5] = mCardapioDoDiaModelList.getOpcao6()+",";
            }
        });
        strOpcao7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[6] = mCardapioDoDiaModelList.getOpcao7()+",";
            }
        });
        strOpcao8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[7] = mCardapioDoDiaModelList.getOpcao8()+",";
            }
        });
        strOpcao9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[8] = mCardapioDoDiaModelList.getOpcao9()+",";
            }
        });
        strOpcao10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[9] = mCardapioDoDiaModelList.getOpcao10()+",";
            }
        });
        strOpcao11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[10] = mCardapioDoDiaModelList.getOpcao11()+",";
            }
        });
        strOpcao12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[11] = mCardapioDoDiaModelList.getOpcao12()+",";
            }
        });
        strOpcao13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[12] = mCardapioDoDiaModelList.getOpcao13()+",";
            }
        });
        strOpcao14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[13] = mCardapioDoDiaModelList.getOpcao14()+",";
            }
        });
        strOpcao15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[14] = mCardapioDoDiaModelList.getOpcao15()+",";
            }
        });
        strOpcao16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOpcaoMarmita[15] = mCardapioDoDiaModelList.getOpcao16();
            }
        });
    }


    private void carregarCapa(ProdutosItem produtosItem) { // carregar String de Json caminho Fotos

        Picasso.with(this)
                .load(produtosItem.getFoto())
                .into(mImageImovel);

        // carregar ARRAY de String de Json para popular ViewPager com FOTOS dos detalhes do ITEM da Lista de Produtos
    }
    private void preencheCampos(ProdutosItem produtosItem){

        strNome.setText(produtosItem.getNome());

        strDescricao.setText(produtosItem.getDescricao());

        txtPreco.setText(produtosItem.getPreco());

    }

    private void configurarBarraDeTitulo(String nome) {
        setSupportActionBar(mToolbar);
       
        if (mAppBar != null){
            if (mAppBar.getLayoutParams() instanceof CoordinatorLayout.LayoutParams){
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mAppBar.getLayoutParams();
                lp.height = getResources().getDisplayMetrics().widthPixels;
            }
        }
        ActionBar actionBar= getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        if (mCollapsingToolbarLayout!=null){
            getSupportActionBar().setDisplayShowTitleEnabled(true);

            //getSupportActionBar().setIcon(R.drawable.ic_launcher);

            mCollapsingToolbarLayout.setTitle(nome);

        } else {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private class ProdutosDownloadTask extends AsyncTask<Void, Void, CardapioDoDiaModel> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //exibirProgresso();
        }

        @Override
        protected CardapioDoDiaModel doInBackground(Void... voids) {
            return ProdutosHttp.obterCardapioDoDia();
        }

        @Override
        protected void onPostExecute(CardapioDoDiaModel produtosItems) {
            super.onPostExecute(produtosItems);
            //mSwipe.setRefreshing(false);
            if (produtosItems!= null){
                mCardapioDoDiaModelList = produtosItems;
                carregarCardapioDoDia();
            }
        }
    }



    private void carregarCardapioDoDia() {

        strOpcao1.setText(mCardapioDoDiaModelList.getOpcao1());
        strOpcao2.setText(mCardapioDoDiaModelList.getOpcao2());
        strOpcao3.setText(mCardapioDoDiaModelList.getOpcao3());
        strOpcao4.setText(mCardapioDoDiaModelList.getOpcao4());
        strOpcao5.setText(mCardapioDoDiaModelList.getOpcao5());
        strOpcao6.setText(mCardapioDoDiaModelList.getOpcao6());
        strOpcao7.setText(mCardapioDoDiaModelList.getOpcao7());
        strOpcao8.setText(mCardapioDoDiaModelList.getOpcao8());
        strOpcao9.setText(mCardapioDoDiaModelList.getOpcao9());
        strOpcao10.setText(mCardapioDoDiaModelList.getOpcao10());
        strOpcao11.setText(mCardapioDoDiaModelList.getOpcao11());
        strOpcao12.setText(mCardapioDoDiaModelList.getOpcao12());
        strOpcao13.setText(mCardapioDoDiaModelList.getOpcao13());
        strOpcao14.setText(mCardapioDoDiaModelList.getOpcao14());
        strOpcao15.setText(mCardapioDoDiaModelList.getOpcao15());
        strOpcao16.setText(mCardapioDoDiaModelList.getOpcao16());
    }


    void calculadoraCarrinho(final ProdutosItem produtosItem){

        final CarrinhoDBRepo carrinhoDBRepo = new CarrinhoDBRepo(getBaseContext());


        btnMais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Integer.parseInt(txtQtde.getText().toString());
                qtdeClick = result+1;
                txtQtde.setText(String.valueOf(qtdeClick));

            }
        });

        btnMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Integer.parseInt(txtQtde.getText().toString());
                txtQtde.setText(String.valueOf(result-1));

            }
        });

        btnCarrinhoADD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (qtdeClick > 0){

                    strGlobal = strDetalhesPedido.getText().toString();

                    if (ehSuco){
                        setDetalhesdoSucoNoPedido();
                    }

                    if (ehMarmita){
                        setDetalhesDaMarmitaPedido();
                    }

                    if (ehBebida){
                        setDetalhesdaBebidaPedido();
                    }


                    if (!strGlobal.isEmpty()) {

                        produtosItem.setDetalhe_Pedido(strGlobal + "," + saidaDetalhePedido);

                    } else {

                        produtosItem.setDetalhe_Pedido(String.valueOf(saidaDetalhePedido));
                    }


                    for (int i =0;i<qtdeClick;i++){


                        carrinhoDBRepo.insert(produtosItem);
                    }
                    if (!String.valueOf(carrinhoDBRepo.getValorTotal()).equals(null)){

                        AlertCarrinho alertCarrinho = new AlertCarrinho(ProdutosDetalhesActivity.this);
                        alertCarrinho.ExecCarrinho();
                    }

                    //Intent it = new Intent(getApplicationContext(),MainActivity.class);
                    //startActivity(it);
                    // Toast.makeText(getApplicationContext(),"SAIDA: " + String.valueOf(carrinhoDBRepo.getProdutos()),Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ProdutosDetalhesActivity.this);
                    alert.setTitle("Caro Cliente")
                            .setMessage("Favor inserir a 'quantidade' que deseja do produto \n Obrigado")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    textQtde.setVisibility(View.GONE);
                                    textQtdeAviso.setVisibility(View.VISIBLE);
                                }
                            });
                    AlertDialog dialog = alert.create();
                    dialog.show();
                }
            }
        });
    }


    private void setDetalhesdaBebidaPedido() {
        saidaDetalhePedido = new StringBuilder();
        saidaDetalhePedido.append(strOpcaoBebida[0]).
                append(strOpcaoBebida[1]);
    }

    private void setDetalhesDaMarmitaPedido() {
        saidaDetalhePedido = new StringBuilder();
        saidaDetalhePedido.append(strOpcaoMarmita[0]).
                append(strOpcaoMarmita[1]).
                append(strOpcaoMarmita[2]).
                append(strOpcaoMarmita[3]).
                append(strOpcaoMarmita[4]).
                append(strOpcaoMarmita[5]).
                append(strOpcaoMarmita[6]).
                append(strOpcaoMarmita[7]).
                append(strOpcaoMarmita[8]).
                append(strOpcaoMarmita[9]).
                append(strOpcaoMarmita[10]).
                append(strOpcaoMarmita[11]).
                append(strOpcaoMarmita[12]).
                append(strOpcaoMarmita[13]).
                append(strOpcaoMarmita[14]).
                append(strOpcaoMarmita[15]);
    }

    private void setDetalhesdoSucoNoPedido() {
        saidaDetalhePedido = new StringBuilder();
        saidaDetalhePedido.append(strOpcaodeSuco[0])
                .append(strOpcaodeSuco[1])
                .append(strOpcaodeSuco[2])
                .append(strOpcaodeSuco[3])
                .append(strOpcaodeSuco[4]);
    }



}
