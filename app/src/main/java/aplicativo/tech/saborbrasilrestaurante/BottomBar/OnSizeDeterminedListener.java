package aplicativo.tech.saborbrasilrestaurante.BottomBar;

/**
 * Created by Malcoln on 11/12/2017.
 */

public interface OnSizeDeterminedListener {
    /**
     * Called when the size of the BottomBar is determined and ready.
     *
     * @param size height or width of the BottomBar, depending on if
     *             the current device is a phone or a tablet.
     */
    void onSizeReady(int size);
}