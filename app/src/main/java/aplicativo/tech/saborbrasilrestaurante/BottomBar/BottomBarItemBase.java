package aplicativo.tech.saborbrasilrestaurante.BottomBar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by Malcoln on 11/12/2017.
 */

public class BottomBarItemBase {
    protected int iconResource;
    protected Drawable icon;
    protected int titleResource;
    protected String title;
    protected int color;

    protected Drawable getIcon(Context context) {
        if (this.iconResource != 0) {
            return ContextCompat.getDrawable(context, this.iconResource);
        } else {
            return this.icon;
        }
    }

    protected String getTitle(Context context) {
        if (this.titleResource != 0) {
            return context.getString(this.titleResource);
        } else {
            return this.title;
        }
    }
}
