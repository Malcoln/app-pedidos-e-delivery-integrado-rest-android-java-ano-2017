package aplicativo.tech.saborbrasilrestaurante;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {

    @BindView(R.id.appBar)
    AppBarLayout mAppBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.strNomePerfil)
    TextView strNomePerfil;

    @BindView(R.id.strEmailLogin)
    TextView strEmailPerfil;

    @BindView(R.id.strTelefonePerfil)
    TextView strTelefonePerfil;

    @BindView(R.id.strEnderecoPerfil)
    TextView strEnderecoPerfil;

    @BindView(R.id.strBairro)
    TextView strBairroPerfil;

    @BindView(R.id.logoffPerfil)
    Button logoffPerfil;

    @BindView(R.id.btnVoltarMenu)
    Button btnVoltarMenu;

    SessionManagerPreConfig session;

    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.perfil_layout, container, false);

        ButterKnife.bind(this,view);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        final ActionBar ab = (ActionBar)((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        session = new SessionManagerPreConfig(getActivity());

        strNomePerfil.setText(session.readNameFromUser());
        strEmailPerfil.setText(session.getEmail());
        strEnderecoPerfil.setText(session.getEnderecoFromUser());
        strBairroPerfil.setText(session.getBairro());
        strTelefonePerfil.setText(session.getTelefone());


        logoffPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setLoginStatus(false);
                Fragment fragment = new LoginFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        String botaoVoltarMenu = btnVoltarMenu.getText().toString();
        SpannableString spannableString = new SpannableString(botaoVoltarMenu);
        spannableString.setSpan(new UnderlineSpan(),0, botaoVoltarMenu.length(),0);
        btnVoltarMenu.setText(spannableString);

        btnVoltarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),MainActivity.class));
            }
        });

        return view;
    }



}
